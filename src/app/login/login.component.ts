import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { Router } from '@angular/router';
import { FetchDataService } from '../services/fetch-data.service';
import { AuthService } from '../services/auth.service';
import {ToastrService} from 'ngx-toastr';
function emailDomainValidator(control: FormControl) {
  let email = control.value;
  if (email && email.indexOf("@") != -1) {
    let [_, domain] = email.split("@");
    if (domain !== "sanofi.com" && domain !== "sanofi-india.com") {
      return {
        emailDomain: {
          parsedDomain: domain
        }
      }
    }
  }
  return null;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
now:any;
  token;
  msg;
  loginForm = new FormGroup({
    email: new FormControl(''),
    passwrds: new FormControl(''),
  });
  coverImage = "../../assets/img/h-about.jpg";
  videoPlay = false;
  potrait = false;
  constructor(private router: Router, private _fd: FetchDataService, private auth: AuthService, private formBuilder:FormBuilder, private toastr:ToastrService) { }

  ngOnInit(): void {
    // Set the date we're counting down to
var countDownDate = new Date("Sep 23, 2021 09:00:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("demo").innerHTML ="Event will be live in <br />"+ days + "Days : " + hours + "Hours : "
  + minutes + "Min : " + seconds + "Sec ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);
    localStorage.setItem('user_guide', 'start');
    localStorage.setItem('ustatus', 'online');
    // this.loginForm = this.formBuilder.group({
    //   email: ['',[Validators.email,
    //    Validators.pattern("[^ @]*@[^ @]*"),
    //       emailDomainValidator]]
    // });
    if (window.innerHeight > window.innerWidth) {
      this.potrait = true;
    } else{
      this.potrait = false;
    }
   //this.updateTimer();
  }
//   updateTimer() {
//     let future = Date.parse("nov 26, 2020 17:30:00");
//     this.now = new Date();
//     let diff = future - this.now;
 
//     let days = Math.floor(diff / (1000 * 60 * 60 * 24));
//     let hours = Math.floor(diff / (1000 * 60 * 60));
//     let mins = Math.floor(diff / (1000 * 60));
//     let secs = Math.floor(diff / 1000);
 
//     let d:any = days;
//     if(d<10){
//       d = '0' + d;
//     }
//     let h:any = hours - days * 24;
//     if(h<10){
//       h = '0' + h;
//     }
//     let m:any = mins - hours * 60;
//     if(m<10){
//       m = '0' + m;
//     }
//     let s:any = secs - mins * 60;
//     if(s<10){
//       s = '0' + s;
//     }
 
//      document.getElementById("timer")
//          .innerHTML =
//         '<span style="background-color:#f9c13b;font-size:2em;border-radius: 5px;">' + d + '</span> ' +
//         '<span style="background-color:#f9c13b;font-size:2em;border-radius: 5px;">' + h + '</span> ' +
//         '<span style="background-color:#f9c13b;font-size:2em;border-radius: 5px;">' + m + '</span> ' +
//         '<span style="background-color:#f9c13b;font-size:2em;border-radius: 5px;">' + s + '</span><br> ' + 
//         '<span style="background-color:#f9c13b;border-radius: 5px;padding: 5px;">Days</span> '+
//         '<span style="background-color:#f9c13b;border-radius: 5px;padding: 5px;">Hrs</span> '+
//         '<span style="background-color:#f9c13b;border-radius: 5px;padding: 5px;">Min</span> '+
//         '<span style="background-color:#f9c13b;border-radius: 5px;padding: 5px;">Secs</span> ';
//  }

//  setInterval('updateTimer()', 1000);


  loggedIn() {
    const user = {
      email: this.loginForm.value.email,
     // password: this.loginForm.get('password').value,
      event_id: 139,
      role_id: 1,
      pass: this.loginForm.value.passwrds,
    };
    console.log(user);
    var isMobile = {
      Android: function() {
          return navigator.userAgent.match(/Android/i);
      },
      BlackBerry: function() {
          return navigator.userAgent.match(/BlackBerry/i);
      },
      iOS: function() {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
      },
      Opera: function() {
          return navigator.userAgent.match(/Opera Mini/i);
      },
      Windows: function() {
          return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
      },
      any: function() {
          return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
      }
      
  };
  
   // console.log("password",user.password)
    this.auth.loginMethod(user).subscribe((res: any) => {
      if (res.code === 1) {
        if( isMobile.iOS() ){
          this.videoPlay = false;
          this.router.navigateByUrl('/lobby');
        }
        this.toastr.success( 'Login Succesfully!');
        this.videoPlay = true;
        let vid: any = document.getElementById('myVideo');
        vid.play();
        localStorage.setItem('virtual', JSON.stringify(res.result));
        //this.videoPlay = true;
       
        
       // this.router.navigateByUrl('/lobby');
        // if (window.innerHeight>window.innerWidth){
        //   this.potrait = true;
        // }else{
        //   this.potrait = false;
        // }
      } else {
        this.msg = 'Invalid Login';
        this.videoPlay = false;
        this.loginForm.reset();
      }
    }, (err: any) => {
      this.videoPlay = false;
      console.log('error', err)
    });
  }
  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerHeight > window.innerWidth) {
      this.potrait = true;
    } else{
      this.potrait = false;
    }
  }
  
  endVideo() {
    this.videoPlay = false;
    //alert('end');
   // this.potrait = false;
    this.router.navigate(['/lobby']);
    // let welcomeAuido:any = document.getElementById('myAudio');
    // welcomeAuido.play();
  }
}
