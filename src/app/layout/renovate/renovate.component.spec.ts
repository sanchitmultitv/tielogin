import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenovateComponent } from './renovate.component';

describe('RenovateComponent', () => {
  let component: RenovateComponent;
  let fixture: ComponentFixture<RenovateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenovateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenovateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
