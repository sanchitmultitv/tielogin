import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;

@Component({
  selector: 'app-renovate',
  templateUrl: './renovate.component.html',
  styleUrls: ['./renovate.component.scss']
})
export class RenovateComponent implements OnInit {
  videoPlayer = 'https://dhzjgfv9krlhb.cloudfront.net/vod/live-vod.mp4/playlist.m3u8';
  constructor(private _fd: FetchDataService) { }
  textMessage = new FormControl('');
  msg;
  qaList;
  interval;
  ngOnInit(): void {
    this.getQA();

  }
  closePopup() {
    $('.r1').modal('hide');
  } 
  getQA(){
    let data = JSON.parse(localStorage.getItem('virtual'));
   //  this._fd.Liveanswers().subscribe((res=>{
   //    console.log(res);
   //    this.qaList = res.result;
   //  }))
  }
  postQuestion(value){
    let data = JSON.parse(localStorage.getItem('virtual'));
    let audi_id = '24'
   console.log(value, data.id, audi_id);
    this._fd.askLiveQuestions(data.id,value,audi_id).subscribe((res=>{
      //console.log(res);
      if(res.code == 1){
        this.msg = 'Submitted Succesfully';
      }
      this.textMessage.reset();
      $('.r1').modal('toggle');
 
    }))
  }
  
  ngOnDestroy() {
    clearInterval(this.interval);
  }
 

}
