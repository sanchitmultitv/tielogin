import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechTwoComponent } from './tech-two.component';

describe('TechTwoComponent', () => {
  let component: TechTwoComponent;
  let fixture: ComponentFixture<TechTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
