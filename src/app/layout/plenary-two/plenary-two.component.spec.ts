import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlenaryTwoComponent } from './plenary-two.component';

describe('PlenaryTwoComponent', () => {
  let component: PlenaryTwoComponent;
  let fixture: ComponentFixture<PlenaryTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlenaryTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlenaryTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
