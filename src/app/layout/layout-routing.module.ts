import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { KbcComponent } from './kbc/kbc.component';
import { CapturePhotoComponent } from './capture-photo/capture-photo.component';
import { AudioScreenComponent } from './audio-screen/audio-screen.component';
import { VisitorProfileComponent } from '../core-components/visitor-profile/visitor-profile.component';
import { Exhibition2Component } from '../core-components/exhibition-hall/exhibition2/exhibition2.component';
import { AgendaComponent } from './agenda/agenda.component';
import { SpeakersComponent } from './speakers/speakers.component';
import { PartnersComponent } from './partners/partners.component';
import { SessionComponent } from './session/session.component';
import { RenovateComponent } from './renovate/renovate.component';
import { PlenaryOneComponent } from './plenary-one/plenary-one.component';
import { PlenaryTwoComponent } from './plenary-two/plenary-two.component';
import { PlenaryThreeComponent } from './plenary-three/plenary-three.component';
import { PlenaryFourComponent } from './plenary-four/plenary-four.component';
import { TechOneComponent } from './tech-one/tech-one.component';
import { TechTwoComponent } from './tech-two/tech-two.component';
import { TechThreeComponent } from './tech-three/tech-three.component';
import { TechFourComponent } from './tech-four/tech-four.component';
import { TechFiveComponent } from './tech-five/tech-five.component';
import { TechSixComponent } from './tech-six/tech-six.component';
import { TechSevenComponent } from './tech-seven/tech-seven.component';
import { TechNineComponent } from './tech-nine/tech-nine.component';
import { TechTenComponent } from './tech-ten/tech-ten.component';
import { TechEightComponent } from './tech-eight/tech-eight.component';
import { TechElevenComponent } from './tech-eleven/tech-eleven.component';
import { TechTwelveComponent } from './tech-twelve/tech-twelve.component';
import { AgendaTwoComponent } from './agenda-two/agenda-two.component';
import { AgendaThreeeComponent } from './agenda-threee/agenda-threee.component';
import { LibraryComponent } from './library/library.component';
import { SessionTwoComponent } from './session-two/session-two.component';
import { SessionThreeComponent } from './session-three/session-three.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { TechThirteenComponent } from './tech-thirteen/tech-thirteen.component';
import { TechFourteenComponent } from './tech-fourteen/tech-fourteen.component';
import { TechFifteenComponent } from './tech-fifteen/tech-fifteen.component';
import { TechSixteenComponent } from './tech-sixteen/tech-sixteen.component';
import { TechSeventeenComponent } from './tech-seventeen/tech-seventeen.component';
import { TechEighteenComponent } from './tech-eighteen/tech-eighteen.component';
import { TechNinteenComponent } from './tech-ninteen/tech-ninteen.component';
import { TechTwentyComponent } from './tech-twenty/tech-twenty.component';
import { TechTwentyoneComponent } from './tech-twentyone/tech-twentyone.component';
import { TechTwentytwoComponent } from './tech-twentytwo/tech-twentytwo.component';
import { TechTwentythreeComponent } from './tech-twentythree/tech-twentythree.component';
import { TechTwentyfourComponent } from './tech-twentyfour/tech-twentyfour.component';
import { TechTwentyfiveComponent } from './tech-twentyfive/tech-twentyfive.component';
import { TechTwentysixComponent } from './tech-twentysix/tech-twentysix.component';
import { TechTwentysevenComponent } from './tech-twentyseven/tech-twentyseven.component';
import { TechTwentyeightComponent } from './tech-twentyeight/tech-twentyeight.component';
import { TechTwentynineComponent } from './tech-twentynine/tech-twentynine.component';
import { TechThirtyComponent } from './tech-thirty/tech-thirty.component';
import { TechThirtyoneComponent } from './tech-thirtyone/tech-thirtyone.component';
import { TechThirtytwoComponent } from './tech-thirtytwo/tech-thirtytwo.component';
import { TechThirtythreeComponent } from './tech-thirtythree/tech-thirtythree.component';
import { ValedictoryComponent } from './valedictory/valedictory.component';
import { HeightlightComponent } from './heightlight/heightlight.component';
import { RenovateTwoComponent } from './renovate-two/renovate-two.component';
import { RenovateThreeComponent } from './renovate-three/renovate-three.component';
import { RenovateLandingComponent } from './renovate-landing/renovate-landing.component';
import { AgendaFourComponent } from './agenda-four/agenda-four.component';
// import { from } from 'core-js/fn/array';



const routes: Routes = [
  { path: '',
    component: LayoutComponent,
    children: [
      { path: '', redirectTo: 'lobby', pathMatch: 'prefix' },
      // {path: 'index', loadChildren: ()=> import('../core-components/index/index.module').then(m => m.IndexModule)},
      {path: 'lobby', loadChildren: ()=> import('../core-components/lobby/lobby.module').then(m => m.LobbyModule)},
      {path: 'welcome', loadChildren: ()=> import('../core-components/welcome-lobby/welcome-lobby.module').then(m => m.WelcomeLobbyModule)},
      {path: 'auditorium', loadChildren: ()=> import('../core-components/auditorium/auditorium.module').then(m => m.AuditoriumModule)},
      {path: 'exhibitionHall', loadChildren: ()=> import('../core-components/exhibition-hall/exhibition-hall.module').then(m => m.ExhibitionHallModule)},
      // {path: 'exhibitionTwo', loadChildren: ()=> import('../core-components/exhibition-two/exhibition-two.module').then(m => m.ExhibitionTwoModule)},
      // {path: 'exhibitionThree', loadChildren: ()=> import('../core-components/exhibition-three/exhibition-three.module').then(m => m.ExhibitionThreeModule)},
      {path: 'registrationDesk', loadChildren: ()=> import('../core-components/registration-desk/registration-desk.module').then(m => m.RegistrationDeskModule)},
      {path: 'networkingLounge', loadChildren: ()=> import('../core-components/networking-lounge/networking-lounge.module').then(m => m.NetworkingLoungeModule)},
      {path: 'stage', loadChildren: ()=> import('../core-components/stage/stage.module').then(m => m.StageModule)},
      {path: 'meetingRoom', loadChildren: ()=> import('../core-components/meeting-room/meeting-room.module').then(m => m.MeetingRoomModule)},
      {path: 'chats', loadChildren: ()=> import('../core-components/chat/chat.module').then(m => m.ChatModule)},
      {path: 'myContacts', loadChildren: ()=> import('../core-components/my-contacts/my-contacts.module').then(m => m.MyContactsModule)},
      {path: 'briefcase', loadChildren: ()=> import('../core-components/briefcase/briefcase.module').then(m => m.BriefcaseModule)},
      {path:'quiz', component:KbcComponent},
      {path:'capturePhoto', component:CapturePhotoComponent},
      {path: 'audioScreen', component:AudioScreenComponent},
      {path: 'profiles', component:VisitorProfileComponent},
      {path: 'agenda', component:AgendaComponent},
      {path: 'speakers', component:SpeakersComponent},
      {path: 'partners', component:PartnersComponent},
      {path: 'sessions', component:SessionComponent},
      {path: 'session-two', component:SessionTwoComponent},
      {path: 'rennovate', component:RenovateLandingComponent},
      {path: 'rennovate-one', component:RenovateComponent},
      {path: 'rennovate-two', component:RenovateTwoComponent},
      {path: 'rennovate-three', component:RenovateThreeComponent},
      {path: 'pavillion', component:HeightlightComponent},
      {path: 'plenary-one', component:PlenaryOneComponent},
      {path: 'plenary-two', component:PlenaryTwoComponent},
      {path: 'plenary-three', component:PlenaryThreeComponent},
      {path: 'plenary-four', component:PlenaryFourComponent},
      {path: 'tech-one', component:TechOneComponent},
      {path: 'tech-two', component:TechTwoComponent},
      {path: 'tech-three', component:TechThreeComponent},
      {path: 'tech-four', component:TechFourComponent},
      {path: 'tech-five', component:TechFiveComponent},
      {path: 'tech-six', component:TechSixComponent},
      {path: 'tech-seven', component:TechSevenComponent},
      {path: 'tech-eight', component:TechEightComponent},
      {path: 'tech-nine', component:TechNineComponent},
      {path: 'tech-ten', component:TechTenComponent},
      {path: 'tech-eleven', component:TechElevenComponent},
      {path: 'tech-twelve', component:TechTwelveComponent},
      {path: 'agenda-two', component:AgendaTwoComponent},
      {path: 'agenda-three', component:AgendaThreeeComponent},
      {path:'agenda-four', component:AgendaFourComponent},
      {path: 'library',component:LibraryComponent},
      {path:'appointments', component:AppointmentsComponent},
      {path:'tech-thirteen', component:TechThirteenComponent},
      {path:'tech-fourteen', component:TechFourteenComponent},
      {path:'tech-fifteen', component:TechFifteenComponent},
      {path:'tech-sixteen', component:TechSixteenComponent},
      {path:'tech-seventeen', component:TechSeventeenComponent},
      {path:'tech-eighteen', component:TechEighteenComponent},
      {path:'tech-ninteen', component:TechNinteenComponent},
      {path:'tech-twenty', component:TechTwentyComponent},
      {path:'tech-twentyone',component:TechTwentyoneComponent},
      {path:'tech-twentytwo', component:TechTwentytwoComponent},
      {path:'tech-twentythree', component:TechTwentythreeComponent},
      {path:'tech-twentyfour', component:TechTwentyfourComponent},
      {path:'tech-twentyfive', component:TechTwentyfiveComponent},
      {path:'tech-twentysix', component:TechTwentysixComponent},
      {path:'tech-twentyseven', component:TechTwentysevenComponent},
      {path:'tech-twentyeight', component:TechTwentyeightComponent},
      {path:'tech-twentynine', component:TechTwentynineComponent},
      {path:'tech-thirty', component:TechThirtyComponent},
      {path:'tech-thirtyone', component:TechThirtyoneComponent},
      {path:'tech-thirtytwo', component:TechThirtytwoComponent},
      {path:'tech-thirtythree', component:TechThirtythreeComponent},
      {path: 'valedictory', component:ValedictoryComponent},
      {path:'session-three', component:SessionThreeComponent}

      // {path: 'exhibitionHall/exhibition2', component:Exhibition2Component},

    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
