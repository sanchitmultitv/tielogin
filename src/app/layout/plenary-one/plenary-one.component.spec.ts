import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlenaryOneComponent } from './plenary-one.component';

describe('PlenaryOneComponent', () => {
  let component: PlenaryOneComponent;
  let fixture: ComponentFixture<PlenaryOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlenaryOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlenaryOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
