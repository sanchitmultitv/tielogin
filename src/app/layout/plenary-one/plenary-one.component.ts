import { Component, OnInit } from '@angular/core';
import { any } from 'underscore';

@Component({
  selector: 'app-plenary-one',
  templateUrl: './plenary-one.component.html',
  styleUrls: ['./plenary-one.component.scss']
})
export class PlenaryOneComponent implements OnInit {
  // videoPlayer:any;
  
   videoPlayer = 'https://dhzjgfv9krlhb.cloudfront.net/vod/plenary-session.mp4/playlist.m3u8';

  isOrange = false;
  iscolor = true;
  constructor() { }

  ngOnInit(): void {
  }
  changeStream(){
    this.videoPlayer = 'https://dhzjgfv9krlhb.cloudfront.net/vod/plenary-session.mp4/playlist.m3u8';
    this.isOrange = false;
    this.iscolor = true;
      }
      changeStreamenglish(){
        this.videoPlayer = 'https://dhzjgfv9krlhb.cloudfront.net/vod/plenary-session.mp4/playlist.m3u8';
    this.iscolor = false;
    this.isOrange = true;
      }
}
