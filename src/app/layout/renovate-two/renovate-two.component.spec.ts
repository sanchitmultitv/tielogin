import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenovateTwoComponent } from './renovate-two.component';

describe('RenovateTwoComponent', () => {
  let component: RenovateTwoComponent;
  let fixture: ComponentFixture<RenovateTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenovateTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenovateTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
