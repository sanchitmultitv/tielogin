import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeightlightComponent } from './heightlight.component';

describe('HeightlightComponent', () => {
  let component: HeightlightComponent;
  let fixture: ComponentFixture<HeightlightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeightlightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeightlightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
