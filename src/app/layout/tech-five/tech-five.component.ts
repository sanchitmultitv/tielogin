import { Component, OnInit } from '@angular/core';
import { techContent } from '../tech-shared/tech.shared';

import {FormControl} from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;
@Component({
  selector: 'app-tech-five',
  templateUrl: './tech-five.component.html',
  styleUrls: ['../tech-shared/tech.shared.scss']
})
export class TechFiveComponent implements OnInit {
  videoPlayer = 'https://d1pb4a4fl471le.cloudfront.net/nn/vod/session8-2020-11-27/playlist.m3u8';
  
  
  sharedContent = techContent;
    slideContent = []; 
  
    
  

constructor(private _fd: FetchDataService) { }
  textMessage = new FormControl('');
  msg;
  qaList;
  interval;
  ngOnInit(): void {
    this.getQA();
    this.slideContent = this.sharedContent;
    this.sharedContent.forEach((ele: any) => {
      this.slideContent.push({ img: ele.img, wd: ele.wd });
    });
  }
  closePopup() {
    $('.t5').modal('hide');
  } 
  getQA(){
    let data = JSON.parse(localStorage.getItem('virtual'));
   //  this._fd.Liveanswers().subscribe((res=>{
   //    console.log(res);
   //    this.qaList = res.result;
   //  }))
  }
  postQuestion(value){
    let data = JSON.parse(localStorage.getItem('virtual'));
    let audi_id = '29'
   console.log(value, data.id, audi_id);
    this._fd.askLiveQuestions(data.id,value,audi_id).subscribe((res=>{
      //console.log(res);
      if(res.code == 1){
        this.msg = 'Submitted Succesfully';
      }
      this.textMessage.reset();
      $('.t5').modal('toggle');
 
    }))
  }
  
  ngOnDestroy() {
    clearInterval(this.interval);
  }
}
