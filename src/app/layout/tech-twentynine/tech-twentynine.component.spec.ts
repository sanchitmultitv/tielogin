import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechTwentynineComponent } from './tech-twentynine.component';

describe('TechTwentynineComponent', () => {
  let component: TechTwentynineComponent;
  let fixture: ComponentFixture<TechTwentynineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechTwentynineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechTwentynineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
