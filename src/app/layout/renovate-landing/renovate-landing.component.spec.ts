import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenovateLandingComponent } from './renovate-landing.component';

describe('RenovateLandingComponent', () => {
  let component: RenovateLandingComponent;
  let fixture: ComponentFixture<RenovateLandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenovateLandingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenovateLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
