import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { ProfileComponent } from './profile/profile.component';
import { AttendeesComponent } from './attendees/attendees.component';
import { AgendaComponent } from './agenda/agenda.component';
import { ChatsComponent } from './chats/chats.component';
import { MyContactsComponent } from './my-contacts/my-contacts.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyFeedbackComponent } from './my-feedback/my-feedback.component';
import { KbcComponent } from './kbc/kbc.component';
import { CapturePhotoComponent } from './capture-photo/capture-photo.component';
import { QuestionComponent } from './question/question.component';
import { PollComponent } from './poll/poll.component';
import {WtsppFileComponent} from './wtspp-file/wtspp-file.component';
import { HeightlightComponent } from './heightlight/heightlight.component';
import { AudioScreenComponent } from './audio-screen/audio-screen.component';
import { BallroomQuestionComponent } from './ballroom-question/ballroom-question.component';
import { GroupChatComponent } from './group-chat/group-chat.component';
import { DocsInfoComponent } from './docs-info/docs-info.component';
import { SalesInfoComponent } from './sales-info/sales-info.component';
import { ScheduleCallComponent } from './schedule-call/schedule-call.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BriefcaseDocsComponent } from './briefcase-docs/briefcase-docs.component';
import { PollTwoComponent } from './poll-two/poll-two.component';
import { PollThreeComponent } from './poll-three/poll-three.component';
import { PollFourComponent } from './poll-four/poll-four.component';
import { BallroomTwoComponent } from './ballroom-two/ballroom-two.component';
import { BallroomThreeComponent } from './ballroom-three/ballroom-three.component';
import { BallroomFourComponent } from './ballroom-four/ballroom-four.component';
import { ActiveAudiComponent } from './active-audi/active-audi.component';
import { GroupChatTwoComponent } from './group-chat-two/group-chat-two.component';
import { GroupChatThreeComponent } from './group-chat-three/group-chat-three.component';
import { GroupChatFourComponent } from './group-chat-four/group-chat-four.component';
import { FaqsComponent } from './faqs/faqs.component';
import { VisitorDashboardComponent } from './visitor-dashboard/visitor-dashboard.component';
import { ExhibitorDirectoryComponent } from './exhibitor-directory/exhibitor-directory.component';
import { Acma1Component } from './acma1/acma1.component';
import { Acma2Component } from './acma2/acma2.component';
import { Acma3Component } from './acma3/acma3.component';
import { Acma4Component } from './acma4/acma4.component';
import { SpeakersComponent } from './speakers/speakers.component';
import { PartnersComponent } from './partners/partners.component';
import { SessionComponent } from './session/session.component';
import { RenovateComponent } from './renovate/renovate.component';
import { PlenaryOneComponent } from './plenary-one/plenary-one.component';
import { PlenaryTwoComponent } from './plenary-two/plenary-two.component';
import { PlenaryThreeComponent } from './plenary-three/plenary-three.component';
import { PlenaryFourComponent } from './plenary-four/plenary-four.component';
import { TechOneComponent } from './tech-one/tech-one.component';
import { TechTwoComponent } from './tech-two/tech-two.component';
import { TechThreeComponent } from './tech-three/tech-three.component';
import { TechFourComponent } from './tech-four/tech-four.component';
import { TechFiveComponent } from './tech-five/tech-five.component';
import { TechSixComponent } from './tech-six/tech-six.component';
import { TechSevenComponent } from './tech-seven/tech-seven.component';
import { TechEightComponent } from './tech-eight/tech-eight.component';
import { TechNineComponent } from './tech-nine/tech-nine.component';
import { TechTenComponent } from './tech-ten/tech-ten.component';
import { TechElevenComponent } from './tech-eleven/tech-eleven.component';
import { TechTwelveComponent } from './tech-twelve/tech-twelve.component';
import { AgendaTwoComponent } from './agenda-two/agenda-two.component';
import { AgendaThreeeComponent } from './agenda-threee/agenda-threee.component';
import { LibraryComponent } from './library/library.component';
import { SessionTwoComponent } from './session-two/session-two.component';
import { VgCoreModule } from 'videogular2/compiled/core';
import { VgControlsModule } from 'videogular2/compiled/controls';
import { VgBufferingModule } from 'videogular2/compiled/buffering';
import { VgOverlayPlayModule } from 'videogular2/compiled/overlay-play';
import { VgStreamingModule } from 'videogular2/compiled/streaming';
import { AppointmentsComponent } from './appointments/appointments.component';
import { TechThirteenComponent } from './tech-thirteen/tech-thirteen.component';
import { TechFourteenComponent } from './tech-fourteen/tech-fourteen.component';
import { TechFifteenComponent } from './tech-fifteen/tech-fifteen.component';
import { TechSixteenComponent } from './tech-sixteen/tech-sixteen.component';
import { TechSeventeenComponent } from './tech-seventeen/tech-seventeen.component';
import { TechEighteenComponent } from './tech-eighteen/tech-eighteen.component';
import { TechNinteenComponent } from './tech-ninteen/tech-ninteen.component';
import { TechTwentyComponent } from './tech-twenty/tech-twenty.component';
import { TechTwentyoneComponent } from './tech-twentyone/tech-twentyone.component';
import { TechTwentytwoComponent } from './tech-twentytwo/tech-twentytwo.component';
import { TechTwentythreeComponent } from './tech-twentythree/tech-twentythree.component';
import { TechTwentyfourComponent } from './tech-twentyfour/tech-twentyfour.component';
import { SessionThreeComponent } from './session-three/session-three.component';
import { TechTwentyfiveComponent } from './tech-twentyfive/tech-twentyfive.component';
import { TechTwentysixComponent } from './tech-twentysix/tech-twentysix.component';
import { TechTwentysevenComponent } from './tech-twentyseven/tech-twentyseven.component';
import { TechTwentyeightComponent } from './tech-twentyeight/tech-twentyeight.component';
import { TechTwentynineComponent } from './tech-twentynine/tech-twentynine.component';
import { TechThirtyComponent } from './tech-thirty/tech-thirty.component';
import { TechThirtyoneComponent } from './tech-thirtyone/tech-thirtyone.component';
import { TechThirtytwoComponent } from './tech-thirtytwo/tech-thirtytwo.component';
import { TechThirtythreeComponent } from './tech-thirtythree/tech-thirtythree.component';
import { DisableComponent } from './disable/disable.component';
import { PavillionComponent } from './pavillion/pavillion.component';
import { ValedictoryComponent } from './valedictory/valedictory.component';
import { RenovateTwoComponent } from './renovate-two/renovate-two.component';
import { RenovateThreeComponent } from './renovate-three/renovate-three.component';
import { RenovateLandingComponent } from './renovate-landing/renovate-landing.component';
import { AgendaFourComponent } from './agenda-four/agenda-four.component';




@NgModule({
  declarations: [
    LayoutComponent,
    ProfileComponent,
    AttendeesComponent,
    AgendaComponent,
    ChatsComponent,
    MyContactsComponent,
    KbcComponent,
    CapturePhotoComponent,
    QuestionComponent,
    PollComponent,
    WtsppFileComponent,
    MyFeedbackComponent, HeightlightComponent, AudioScreenComponent, BallroomQuestionComponent, GroupChatComponent, DocsInfoComponent, SalesInfoComponent, ScheduleCallComponent, BriefcaseDocsComponent, PollTwoComponent, PollThreeComponent, PollFourComponent, BallroomTwoComponent, BallroomThreeComponent, BallroomFourComponent, ActiveAudiComponent, GroupChatTwoComponent, GroupChatThreeComponent, GroupChatFourComponent, FaqsComponent, VisitorDashboardComponent, Acma1Component, Acma2Component, Acma3Component, Acma4Component,ExhibitorDirectoryComponent, SpeakersComponent, PartnersComponent, SessionComponent, RenovateComponent, PlenaryOneComponent, PlenaryTwoComponent, PlenaryThreeComponent, PlenaryFourComponent, TechOneComponent, TechTwoComponent, TechThreeComponent, TechFourComponent, TechFiveComponent, TechSixComponent, TechSevenComponent, TechEightComponent, TechNineComponent, TechTenComponent, TechElevenComponent, TechTwelveComponent, AgendaTwoComponent, AgendaThreeeComponent, LibraryComponent, SessionTwoComponent, AppointmentsComponent, TechThirteenComponent, TechFourteenComponent, TechFifteenComponent, TechSixteenComponent, TechSeventeenComponent, TechEighteenComponent, TechNinteenComponent, TechTwentyComponent, TechTwentyoneComponent, TechTwentytwoComponent, TechTwentythreeComponent, TechTwentyfourComponent, SessionThreeComponent, TechTwentyfiveComponent, TechTwentysixComponent, TechTwentysevenComponent, TechTwentyeightComponent, TechTwentynineComponent, TechThirtyComponent, TechThirtyoneComponent, TechThirtytwoComponent, TechThirtythreeComponent, DisableComponent, PavillionComponent, ValedictoryComponent, RenovateTwoComponent, RenovateThreeComponent, RenovateLandingComponent, AgendaFourComponent],
exports:[MyFeedbackComponent, GroupChatComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutRoutingModule,
    NgbModule,
    VgCoreModule,
    VgControlsModule,
    VgBufferingModule,
    VgOverlayPlayModule,
    VgStreamingModule

  ]
})
export class LayoutModule { }
