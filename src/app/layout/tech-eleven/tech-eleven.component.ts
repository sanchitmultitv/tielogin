import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;
import { techContent } from '../tech-shared/tech.shared';

@Component({
  selector: 'app-tech-eleven',
  templateUrl: './tech-eleven.component.html',
  styleUrls: ['../tech-shared/tech.shared.scss']
})
export class TechElevenComponent implements OnInit {
  videoPlayer = 'https://dhzjgfv9krlhb.cloudfront.net/vod/session32/playlist.m3u8';
  
  
  sharedContent = techContent;
    slideContent = []; 
  
    
  

constructor(private _fd: FetchDataService) { }
  textMessage = new FormControl('');
  msg;
  qaList;
  interval;
  ngOnInit(): void {
    this.getQA();
    this.slideContent = this.sharedContent;
    this.sharedContent.forEach((ele: any) => {
      this.slideContent.push({ img: ele.img, wd: ele.wd });
    });
  }
  closePopup() {
    $('.t11').modal('hide');
  } 
  getQA(){
    let data = JSON.parse(localStorage.getItem('virtual'));
   //  this._fd.Liveanswers().subscribe((res=>{
   //    console.log(res);
   //    this.qaList = res.result;
   //  }))
  }
  postQuestion(value){
    let data = JSON.parse(localStorage.getItem('virtual'));
    let audi_id = '35'
   console.log(value, data.id, audi_id);
    this._fd.askLiveQuestions(data.id,value,audi_id).subscribe((res=>{
      //console.log(res);
      if(res.code == 1){
        this.msg = 'Submitted Succesfully';
      }
      this.textMessage.reset();
      $('.t11').modal('toggle');
 
    }))
  }
  
  ngOnDestroy() {
    clearInterval(this.interval);
  }
}
