import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechElevenComponent } from './tech-eleven.component';

describe('TechElevenComponent', () => {
  let component: TechElevenComponent;
  let fixture: ComponentFixture<TechElevenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechElevenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechElevenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
