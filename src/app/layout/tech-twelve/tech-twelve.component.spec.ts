import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechTwelveComponent } from './tech-twelve.component';

describe('TechTwelveComponent', () => {
  let component: TechTwelveComponent;
  let fixture: ComponentFixture<TechTwelveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechTwelveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechTwelveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
