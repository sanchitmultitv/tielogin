import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechTwentyfiveComponent } from './tech-twentyfive.component';

describe('TechTwentyfiveComponent', () => {
  let component: TechTwentyfiveComponent;
  let fixture: ComponentFixture<TechTwentyfiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechTwentyfiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechTwentyfiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
