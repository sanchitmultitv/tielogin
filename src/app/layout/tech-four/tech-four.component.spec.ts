import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechFourComponent } from './tech-four.component';

describe('TechFourComponent', () => {
  let component: TechFourComponent;
  let fixture: ComponentFixture<TechFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
