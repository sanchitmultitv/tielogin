import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechEightComponent } from './tech-eight.component';

describe('TechEightComponent', () => {
  let component: TechEightComponent;
  let fixture: ComponentFixture<TechEightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechEightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechEightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
