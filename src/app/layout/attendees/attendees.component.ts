import { Component, OnInit, QueryList, ElementRef, ViewChildren, Renderer2 } from '@angular/core';
import { FetchDataService } from '../../services/fetch-data.service';
import { FormGroup, FormControl } from '@angular/forms';
import { coreBusinessArr, coreSelectorArr } from '../../shared/attendeesforsearch';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;
@Component({
  selector: 'app-attendees',
  templateUrl: './attendees.component.html',
  styleUrls: ['./attendees.component.scss']
})
export class AttendeesComponent implements OnInit {
  attendees = [];
  model: NgbDateStruct;
  date: { year: number, month: number };
  newTimeSlots: any = [];
  timeVal;
  uids;
  names: string;
  offset=50;
  coreSelectorArr: any = coreSelectorArr;
  coreBusinessArr: any = coreBusinessArr;
  core_sector = new FormControl('');
  core_business = new FormControl('');
  @ViewChildren("checkboxes") checkboxes: QueryList<ElementRef>;

  constructor(private _fd: FetchDataService, private calendar: NgbCalendar, private renderer: Renderer2,private toastr: ToastrService) { }

  form = new FormGroup({
    core_sector: new FormControl(''),
    core_business: new FormControl('')
  });
  onSubmit() {
    if (this.form.invalid) {
      return;
    } else {
      this._fd.getReinvestAttendees(139, this.form.value.core_sector, this.form.value.core_business).subscribe((res: any) => {
        this.attendees = res.result;
        this.form.reset();
      });
    }

  }
  getUserid(id) {
    this.uids = id;
  }
  closeCall() {
    $('.scheduleCallmodal').modal('hide');
  }
  ngOnInit(): void {
    this.model = this.calendar.getToday();
    // console.log('todaydate', this.model);
    let myDate = this.model.year + '-' + this.model.month + '-' + this.model.day;
    this.core_sector.setValue(this.coreSelectorArr[0].name);
    this.core_business.setValue(this.coreBusinessArr[0].name);

    this.form.reset();
    let event_id = 139;
    this._fd.getAttendees(event_id,this.offset).subscribe((res: any) => {
      this.attendees = res.result;
    });
    this._fd.totalTimeSlots(this.uids, myDate).subscribe(res => {
      console.log('timeresponse', res);
      this.newTimeSlots = res.result;
    });
  }
  loadmore(){
    this.offset = this.offset + 50;
   // alert(this.offset);
    let event_id = 139;
    // this.sender_id = JSON.parse(localStorage.getItem('virtual')).id;
    // this.sender_name = JSON.parse(localStorage.getItem('virtual')).name;
    
    this._fd.getAttendees(event_id, this.offset).subscribe((res: any) => {
      // console.log('chatlist', res.result)
      let arr: any = res.result;
      for(let i =0; i<arr.length; i++){
        console.log(arr[i]);
        this.attendees.push(arr[i]);
      }
     // this.allChatList.push(res.result);

     // console.log('nxt array',res.result);
      // this.searchChatList = res.result;
      // this.receiver_id = res.result[0].id;
      // this.receiver_name = res.result[0].name;
      // this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
      //   this.chatMessage = res.result;
      // });

      // this.timer = setInterval(() => {
      //   this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
      //     this.chatMessage = res.result;
      //   });

      // }, 150000);
      // this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
      //   this.chatMessage = res.result;
      // });

    });
  }
  // filterData(value) {
  //   let event_id = 139;
  //   if (value.length >= 3) {
  //     this._fd.getAttendeesbyName(event_id, value).subscribe((res: any) => {
  //       this.attendees = res.result;
  //       console.log(this.attendees);
  //     });
  //   }
  //   else {
  //     this._fd.getAttendees(event_id).subscribe((res: any) => {
  //       this.attendees = res.result;
  //     });
  //   }
  // }
  filterData(query) {
    let event_id = 139;    
      if(query!==''){
        if ((this.coreSectorvalue === null) && (this.coreBusinessvalue === null)) {
          this._fd.getAttendeesbyName(event_id, query).subscribe(res => {
            this.attendees = res.result;
          });
        }
        if ((this.coreSectorvalue !== null) && (this.coreBusinessvalue === null)) {
          this._fd.ReinvestAttendeesCoreSectorSearch(event_id, this.coreSectorvalue, query).subscribe((res:any) => {
            this.attendees = res.result;
          });
        }
        if ((this.coreSectorvalue === null) && (this.coreBusinessvalue !== null)) {
          this._fd.ReinvestAttendeesCoreBusinessSearch(event_id,this.coreBusinessvalue,query).subscribe((res:any) => {
            this.attendees = res.result;
          });
        }
        if ((this.coreSectorvalue !== null) && (this.coreBusinessvalue !== null)) {
          this._fd.ReinvestAttendees(event_id,this.coreSectorvalue, this.coreBusinessvalue,query).subscribe((res:any) => {
            this.attendees = res.result;
          });
        }
    }else {
      if ((this.coreSectorvalue === null) && (this.coreBusinessvalue === null)) {
        this._fd.getAttendees(event_id, this.offset).subscribe(res => {
          this.attendees = res.result;
        });
      }
      if ((this.coreSectorvalue !== null) && (this.coreBusinessvalue === null)) {
        this._fd.getReinvestAttendeesCoreSector(event_id, this.coreSectorvalue).subscribe((res:any) => {
          this.attendees = res.result;
        });
      }
      if ((this.coreSectorvalue === null) && (this.coreBusinessvalue !== null)) {
        this._fd.getReinvestAttendeesCoreBusiness(event_id,this.coreBusinessvalue).subscribe((res:any) => {
          this.attendees = res.result;
        });
      }
      if ((this.coreSectorvalue !== null) && (this.coreBusinessvalue !== null)) {
        this._fd.getReinvestAttendees(event_id,this.coreSectorvalue, this.coreBusinessvalue).subscribe((res:any) => {
          this.attendees = res.result;
        });
      }
    }
  }

  coreSectorvalue = null;
  coreBusinessvalue = null;
  onSelectChange(value, core) {
    let event_id=139;
    if(core==='csect'){
      this.coreSectorvalue=this.core_sector.value;
    }
    if(core==='cbusi'){
      this.coreBusinessvalue=this.core_business.value;
    }    
    console.log('vlaue', value, core, this.coreSectorvalue, this.coreBusinessvalue);
    if ((this.coreSectorvalue !== null) && (this.coreBusinessvalue === null)) {
      this._fd.getReinvestAttendeesCoreSector(event_id, this.coreSectorvalue).subscribe((res:any) => {
        this.attendees = res.result;
      });
    }
    if ((this.coreSectorvalue === null) && (this.coreBusinessvalue !== null)) {
      this._fd.getReinvestAttendeesCoreBusiness(event_id,this.coreBusinessvalue).subscribe((res:any) => {
        this.attendees = res.result;
      });
    }
    if ((this.coreSectorvalue !== null) && (this.coreBusinessvalue !== null)) {
      this._fd.getReinvestAttendees(event_id,this.coreSectorvalue, this.coreBusinessvalue).subscribe((res:any) => {
        this.attendees = res.result;
      });
    }
    this.names = '';
  }
  clearAll() {
    this.coreSectorvalue = null;
    this.coreBusinessvalue = null;
    this.ngOnInit();
  }
  closePopup() {
    $('.attendeesModal').modal('hide');
  }
  getTime(event: any, valClass, time) {
    // console.log(time);
    this.timeVal = time;
    const hasClass = event.target.classList.contains(valClass);
    $(".time-list li a.active").removeClass("active");
    // adding classname 'active' to current click li
    this.renderer.addClass(event.target, valClass);
    // if (hasClass) {
    //   //alert('has')
    //       this.renderer.removeClass(event.target, valClass);
    //     } else {
    //      // alert(valClass)
    //       this.renderer.addClass(event.target, valClass);
    //     }
  }
  confirm() {
    // alert(this.dateModel);
    //console.log('val',dp);
    //this.route.paramMap.subscribe(params => {
    //this.exhibitionName = params.get('exhibitName');
    let data = JSON.parse(localStorage.getItem('virtual'));
    console.log(this.model.year + '-' + this.model.month + '-' + this.model.day + ' ' + this.timeVal);
    const CallData = new FormData();
    CallData.append('attendee_id', this.uids);
    CallData.append('user_id', data.id);
    CallData.append('time', this.model.year + '-' + this.model.month + '-' + this.model.day + ' ' + this.timeVal);
    this._fd.schdeuleAcallattende(CallData).subscribe(res => {
      console.log(res);
      if (res.code == 1) {
        this.toastr.success( 'Call scheduled succesfully!');

        let myDate = this.model.year + '-' + this.model.month + '-' + this.model.day;
        //let myDate = '2020-10-25';
        let data = JSON.parse(localStorage.getItem('virtual'));
        this._fd.totalTimeSlots(this.uids, myDate).subscribe(res => {
          console.log('timeresponse', res);
          this.newTimeSlots = res.result;
        })
        setTimeout(() => {
          $('.scheduleCallmodal').modal('hide');
        }, 2000);
      }

    });

    //});
  }
  onDateSelect(dates) {
    console.log('aycycyt', dates.year + '-' + dates.month + '-' + dates.day);
    let myDate = dates.year + '-' + dates.month + '-' + dates.day;
    //this.route.paramMap.subscribe(params => {
    //this.exhibitionName = params.get('exhibitName');
    let data = JSON.parse(localStorage.getItem('virtual'));
    this._fd.totalTimeSlots(this.uids, myDate).subscribe(res => {
      console.log('timeresponse', res);
      this.newTimeSlots = res.result;
    })
    //});


  }
}
