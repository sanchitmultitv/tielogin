import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechTenComponent } from './tech-ten.component';

describe('TechTenComponent', () => {
  let component: TechTenComponent;
  let fixture: ComponentFixture<TechTenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechTenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechTenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
