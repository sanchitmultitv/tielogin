import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechTwentysevenComponent } from './tech-twentyseven.component';

describe('TechTwentysevenComponent', () => {
  let component: TechTwentysevenComponent;
  let fixture: ComponentFixture<TechTwentysevenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechTwentysevenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechTwentysevenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
