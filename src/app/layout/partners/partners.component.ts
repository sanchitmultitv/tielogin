import { Component, OnInit } from '@angular/core';
import { techContent } from '../tech-shared/tech.shared';

@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.scss']
})
export class PartnersComponent implements OnInit {
  sharedContent = techContent;
  slideContent = []; 
  constructor() { }

  ngOnInit(): void {
    this.slideContent = this.sharedContent;
    this.sharedContent.forEach((ele: any) => {
      this.slideContent.push({ img: ele.img, wd: ele.wd });
    });
  }

}
