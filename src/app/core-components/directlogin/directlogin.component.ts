import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-directlogin',
  templateUrl: './directlogin.component.html',
  styleUrls: ['./directlogin.component.scss']
})
export class DirectloginComponent implements OnInit {
user;
  constructor(private route:ActivatedRoute,private auth: AuthService,private router:Router) { }

  ngOnInit(): void {
    this.getemail();
  }
getemail(){
  this.user = this.route.snapshot.queryParamMap.get('email');
  console.log(this.user);
  const user = {
    email: this.user,
   // password: this.loginForm.get('password').value,
    event_id: 57,
    role_id: 1,
    //pass: this.loginForm.value.passwrds,
  };
  this.auth.loginMethod(user).subscribe((res: any) => {
    if (res.code === 1) {
      // if( isMobile.iOS() ){
      //   this.videoPlay = false;
      //   this.router.navigateByUrl('/lobby');
      // }
      //this.toastr.success( 'Login Succesfully!');
      //this.videoPlay = true;
      localStorage.setItem('virtual', JSON.stringify(res.result));
      this.router.navigateByUrl('/lobby');
      //this.videoPlay = true;
     // let vid: any = document.getElementById('myVideo');
     // vid.play();
      
   //   this.router.navigateByUrl('/lobby');
      // if (window.innerHeight>window.innerWidth){
      //   this.potrait = true;
      // }else{
      //   this.potrait = false;
      // }
    } else {
      //this.msg = 'Invalid Login';
      //this.videoPlay = false;
      //this.loginForm.reset();
      this.router.navigateByUrl('/login');
    }
  }, (err: any) => {
    //this.videoPlay = false;
    console.log('error', err)
  });
}
}

