import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { fadeAnimation } from '../../shared/animation/fade.animation';

@Component({
  selector: 'app-auditorium',
  templateUrl: './auditorium.component.html',
  styleUrls: ['./auditorium.component.scss'],
  animations: [fadeAnimation]
})
export class AuditoriumComponent implements OnInit {
  videoEnd = false;
  constructor() { }

  ngOnInit(): void {
    
  }

  videoEnded() {
    this.videoEnd = true;
  }
}
