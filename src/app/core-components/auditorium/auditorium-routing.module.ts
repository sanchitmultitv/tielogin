import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuditoriumComponent } from './auditorium.component';
import { LeftAuditoriumComponent } from './components/left-auditorium/left-auditorium.component';
import { RightAuditoriumComponent } from './components/right-auditorium/right-auditorium.component';
import { componentFactoryName } from '@angular/compiler';
import { FrontAuditoriumDeskComponent } from './components/front-auditorium-desk/front-auditorium-desk.component';
import { FrontAuditoriumComponent } from './components/front-auditorium/front-auditorium.component';
import { FourthAuditoriumComponent } from './components/fourth-auditorium/fourth-auditorium.component';
import { AuditoriumThreeModule } from './components/auditorium-three/auditorium-three.module';
import { AuditoriumFourModule } from './components/auditorium-four/auditorium-four.module';


const routes: Routes = [
  {path:'',
  component: AuditoriumComponent,
  children:[
    {path:'', redirectTo:'left'},
    {path:'left', component:LeftAuditoriumComponent},
    {path:'right', component:RightAuditoriumComponent},
    {path:'front-desk', component:FrontAuditoriumDeskComponent},
    {path: 'fourth-auditorium', component:FourthAuditoriumComponent},
    {path: 'front-auditorium', component:FrontAuditoriumComponent},
    {path: 'one', loadChildren: ()=> import('./components/auditorium-one/auditorium-one.module').then(m => m.AuditoriumOneModule)},
    {path:'two/:id', loadChildren: ()=> import('./components/auditorium-two/auditorium-two.module').then(m => m.AuditoriumTwoModule)},
    {path:'three', loadChildren: ()=> import('./components/auditorium-three/auditorium-three.module').then(m => m.AuditoriumThreeModule)},
    {path:'four', loadChildren: ()=> import('./components/auditorium-four/auditorium-four.module').then(m=> m.AuditoriumFourModule)},

  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditoriumRoutingModule { }
