import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuditoriumOneRoutingModule } from './auditorium-one-routing.module';
import { AuditoriumOneComponent } from './auditorium-one.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VgCoreModule } from 'videogular2/compiled/core';
import { VgControlsModule } from 'videogular2/compiled/controls';
import { VgBufferingModule } from 'videogular2/compiled/buffering';
import { VgOverlayPlayModule } from 'videogular2/compiled/overlay-play';
import { VgStreamingModule } from 'videogular2/compiled/streaming';


@NgModule({
  declarations: [AuditoriumOneComponent],
  imports: [
    CommonModule,
    AuditoriumOneRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    VgCoreModule,
    VgControlsModule,
    VgBufferingModule,
    VgOverlayPlayModule,
    VgStreamingModule
  ]
})
export class AuditoriumOneModule { }
