import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExhibitionTwoRoutingModule } from './exhibition-two-routing.module';
import { ExhibtionTwoComponent } from './exhibtion-two/exhibtion-two.component';


@NgModule({
  declarations: [ExhibtionTwoComponent],
  imports: [
    CommonModule,
    ExhibitionTwoRoutingModule
  ]
})
export class ExhibitionTwoModule { }
