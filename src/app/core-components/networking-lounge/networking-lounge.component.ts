import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;
declare var twttr: any;
declare var instgrm: any;
@Component({
  selector: 'app-networking-lounge',
  templateUrl: './networking-lounge.component.html',
  styleUrls: ['./networking-lounge.component.scss']
})
export class NetworkingLoungeComponent implements OnInit {
  videoEnd=false;
  liveMsg= false;
  ChatMsg = false;
  senderName;
  redirect;
  videoPlayer = '../assets/video/networking_lounge_video.mp4';
  constructor(private router: Router, private chat: ChatService) { }

  ngOnInit(): void {
    twttr.widgets.load();
    instgrm.Embeds.process()

    $("#twitter-widget-0").contents().find(".twitter-timeline").attr("style","width:100% !important; height:50px !important");

    this.chat.getconnect('toujeo-139');
    this.chat.getMessages().subscribe((data=>{
    //  console.log('data',data);
    let notify = data.split('_');
    if(notify[0] == 'start' && notify[1] == 'live'){
  this.liveMsg = true;
  this.redirect = notify[2];
 }
      // tslint:disable-next-line: align
      if(data == 'stop_live'){
        this.liveMsg = false;
      }
      else{
        let mydata = JSON.parse(localStorage.getItem('virtual'));
        let getMsg = data.split('_');
        console.log('chats',getMsg);
        if(getMsg[0] == "one2one" && getMsg[1] == mydata.id){
          this.senderName = getMsg[2];
          this.ChatMsg = true;
        }
        setTimeout(() => {
          this.ChatMsg = false;
        }, 4000);
      }
    }))
  }
  videoEnded() {
    this.videoEnd = true;
  }
  openWtsapp(){
    $('.wtsappModal').modal('show');
  }
  openChat(){
    $('.chatsModal').modal('show');
  }
  openCamera(){
    this.router.navigate(['/capturePhoto']);
  }
  heading;
  openwidgetpopup(value){
    this.heading=value;
    $('.widgetmodal').modal('show');
  }
  closedwidgetpopup(){
    $('.widgetmodal').modal('hide');
  }
}
