import { Component, OnInit, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {FormControl} from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import  * as html2canvas  from 'html2canvas';
import {ToastrService} from 'ngx-toastr';

import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;

@Component({
  selector: 'app-himachal',
  templateUrl: './himachal.component.html',
  styleUrls: ['./himachal.component.scss']
})
export class HimachalComponent implements OnInit {
  textMessage = new FormControl('');
  msg;
  qaList;
  interval;
  pord_desc;
breif_flag = false;
exhibition_id = '0';
exhibiton: any = [];
brouchers: any = [];
graphic:any;
exhibitionName='212';
  banner;
  product;
salesPerson: any = [];
model: NgbDateStruct;
date: {year: number, month: number};
newTimeSlots:any=[];
timeVal;
boolTime= false;
isShow = true;
newMessage: string;
  msgs: string;
  newMSg =[];
  showDiv = true;
  showchat = true;
  hidechat = false;
  messageList = [];
  roomName: string;
  myId;
  uName;
  myid;
@ViewChild('screen') screen: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('downloadLink') downloadLink: ElementRef;
  constructor(private _fd: FetchDataService, private route: ActivatedRoute, private toastr: ToastrService,private calendar: NgbCalendar, private renderer: Renderer2, private chat: ChatService) { }

  ngOnInit(): void {
    this.banner = 'assets/reinvest/stateall/himachal.jpg';
    
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    this.myid = virtual.id;
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    let action = 'click_exhibition_HIMACHAL PRADESH';
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    if(virtual.company == ''){
      virtual.company = 'others';
    };
    const formData = new FormData();
    formData.append('event_id', virtual.event_id);
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', virtual.company);
    formData.append('designation', virtual.designation);
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });

    this.getQA();
    this.chat.getconnect('toujeo-57');
    this.chat.getMessages().subscribe((data => {
      console.log('socketdata', data);
  
      let check = data.split('_');
       if(check[0] == 'one2one' && check[1]== this.myid) {
        //alert(data);
         this.getQA();
       }
    }));
   // this.getExhibitionData();
    this.model = this.calendar.getToday();
    console.log('todaydate',this.model)
    this.getQA();
    this.chat.getconnect('toujeo-57');
    this.chat.getMessages().subscribe((data=>{
       console.log('socketdata', data);
     
       if(data == 'one2one') {
        //alert(data);
         this.getQA();
       }
     }));
    let myDate = this.model.year+'-'+this.model.month+'-'+this.model.day;
    //let myDate = '2020-10-25';
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.myId = data.id;
    this._fd.totalTimeSlots(this.exhibitionName,myDate).subscribe(res=>{
       console.log('timeresponse',res);
       this.newTimeSlots=res.result;
     })
  }

  playShowVideo() {
    let playVideo: any = document.getElementById("video");
    playVideo.play();
    $('#playVideo').modal('show');
  }
  
  closeModalVideo(){
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }
  getExhibitionData(){
    let data = JSON.parse(localStorage.getItem('virtual'));
    //  this.exhibitionName = '';
      this._fd.getExhibition(this.exhibitionName, 'country').subscribe(res => {
        //console.log('exhibition',res);
        this.exhibiton = res.result;
      //this.graphic = res.result[0].graphics;
        //this.banner = 'https://virtualapi.multitvsolution.com/exhibitions/standard.jpeg'
        this.salesPerson = res.result[0].sales;
       // this.product = res.result[0].product[0].url;
        //this.pord_desc =res.result[0].product[0].desc;
        console.log(this.banner, 'vauvduyvuduv')
      });
      // this._fd.getBrouchers(this.exhibitionName,data.id).subscribe(res=> {
      //   console.log(res.result);
      //   this.brouchers = res.result;
      // })
      
  
   
  }
  sendMessage() {
    this.roomName = 'ddbjb1bk';
    if (this.msgs != '') {
      this.chat.sendMessage(this.msgs, this.uName, this.roomName);
      this.msgs = '';
    }
  }
  hidediv() {
    this.showDiv = false;
    this.showchat = false;
    this.hidechat = true;
      }
      show() {
        this.showDiv = true;
        this.hidechat = false;
    this.showchat = true;
          }
          getUser() {
            let user = JSON.parse(localStorage.getItem('virtual'));
            this.uName = user.name;
          //  console.log(this.uName);
           // let room = JSON.parse(localStorage.getItem('room_id'));
            //console.log(room);
            this.roomName = 'ddbjb1bk';
            this.chat.addUser(this.uName, this.roomName);
          }
      getQA(){
          //this.exhibitionName = '';
        //  console.log('exhibitonid',this.exhibition_id);
          let data = JSON.parse(localStorage.getItem('virtual'));
         // console.log('uid',data.id);
          this._fd.getanswers(data.id,this.exhibitionName).subscribe((res=>{
            //console.log(res);
            this.qaList = res.result;
            // alert('hello');
          }))
      
        }
        postQuestion(value) {
           // this.exhibitionName ='';
            let data = JSON.parse(localStorage.getItem('virtual'));
         console.log('value',value);
        // this.getQA();
        if(value != undefined){
            this._fd.askQuestions(data.id,data.name, value,this.exhibitionName,'Himachal Stall').subscribe((res=>{
            if(res.code == 1) {
              this.msg = 'Submitted Succesfully';
            // var d = $('.chat_message');
            // d.scrollTop(d.prop("scrollHeight"))
            }
            this.getQA();
             
            setTimeout(() => {
              $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight-100;
              this.msg = '';
      //$('.liveQuestionModal').modal('hide');
            }, 2000);
            // setTimeout(() => {
            //   this.msg = '';
            //   $('.liveQuestionModal').modal('hide');
            // }, 2000);
            this.textMessage.reset();
          }))
        }
        
      
        }
        changeProdcut(prod,descs){
          this.product = prod;
          this.pord_desc =descs;
        }
        closeproduct(){
          $('.productModal').modal('hide');
        }
        closeChat(){
          $('.liveQuestionModal').modal('hide');
        }
        closeright(){
          $('.rightGraphcModal').modal('hide');
        }
        closeLeft(){
          $('.leftGraphicModal').modal('hide');
        }
        cardPost(){
          let data = JSON.parse(localStorage.getItem('virtual'));
      
          const formsData = new FormData();
          formsData.append('user_id', data.id);
          formsData.append('exhibition_id',this.exhibiton[0].id );
          this._fd.postCard(formsData).subscribe((res:any)=>{
            console.log(res,'ressssd');
            if(res.code == 1){
              this.toastr.success( 'Card dropped successfully !!');
            }
            
          });
          //this.toastr.success( 'Card dropped successfully !!');
        }
        getTime(event: any, valClass, time) {
          // console.log(time);
          this.timeVal = time;
          const hasClass = event.target.classList.contains(valClass);
          $(".time-list li a.active").removeClass("active");
          // adding classname 'active' to current click li
          this.renderer.addClass(event.target, valClass);
          // if (hasClass) {
          //   //alert('has')
          //       this.renderer.removeClass(event.target, valClass);
          //     } else {
          //      // alert(valClass)
          //       this.renderer.addClass(event.target, valClass);
          //     }
            }
            closePopup(){
              $('.docsModal').modal('hide');
                }
                confirm() {
                  // alert(this.dateModel);
               //console.log('val',dp);
               // this.exhibitionName = params.get('exhibitName');
                let data = JSON.parse(localStorage.getItem('virtual'));
                console.log(this.model.year+'-'+this.model.month+'-'+this.model.day+' '+this.timeVal);
                const CallData = new FormData();
                CallData.append('exhibition_id', this.exhibitionName);
                CallData.append('user_id', data.id);
                CallData.append('time', this.model.year+'-'+this.model.month+'-'+this.model.day+' '+this.timeVal);
                this._fd.schdeuleAcall(CallData).subscribe(res=>{
                 console.log(res);
                 if(res.code == 1){
                  this.toastr.success( 'Call scheduled succesfully!');
              
                  let myDate = this.model.year+'-'+this.model.month+'-'+this.model.day;
                  //let myDate = '2020-10-25';
                  let data = JSON.parse(localStorage.getItem('virtual'));
                  this._fd.totalTimeSlots(this.exhibitionName,myDate).subscribe(res=>{
                     console.log('timeresponse',res);
                     this.newTimeSlots=res.result;
                   })
                  setTimeout(() => {
                   $('.scheduleCallmodal').modal('hide');
                  }, 2000);
                 }
                
               });
               
              
                 }
                onDateSelect(dates){
                  console.log('aycycyt',dates.year+'-'+dates.month+'-'+dates.day);
                  let myDate = dates.year+'-'+dates.month+'-'+dates.day;
                   // this.exhibitionName = params.get('exhibitName');
                    let data = JSON.parse(localStorage.getItem('virtual'));
                    this._fd.totalTimeSlots(this.exhibitionName,myDate).subscribe(res=>{
                      console.log('timeresponse',res);
                      this.newTimeSlots=res.result;
                    })
                  
                  
              
                }
                closesales(){
                  $('.salesModal').modal('hide');
                    }
                    closeCall(){
                      $('.scheduleCallmodal').modal('hide');
                    }
                    openChat(){
                      $('.liveQuestionModal').modal('show');
                    }
                    download(i, name){
                      // html2canvas(this.screen.nativeElement).then(canvas => {
                      //   this.canvas.nativeElement.src = canvas.toDataURL();
                      //   this.downloadLink.nativeElement.href = canvas.toDataURL('image/png');
                      //   this.downloadLink.nativeElement.download = name + '.png';
                      //   this.downloadLink.nativeElement.click();
                        
                      // });
                      // this.toastr.success( 'Card Dropped Succesfully!');
                      var container = document.getElementById("hello" + i); //specific element on page
                    //var container = document.body; // full page 
                    // tslint:disable-next-line: indent
                    // tslint:disable-next-line: align
                    html2canvas(container).then(function(canvas) {
                              var link = document.createElement("a");
                              document.body.appendChild(link);
                              link.download = name + ".png";
                              link.href = canvas.toDataURL('image/png');
                              link.target = '_blank';
                              link.click();
                          });
                         // this.toastr.success( 'Card Dropped Succesfully!');
                    }

}
