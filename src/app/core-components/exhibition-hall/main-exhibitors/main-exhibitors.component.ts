import { Component, OnInit, ElementRef, ViewChild, Renderer2, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, RouterLinkWithHref } from '@angular/router';
import { FormControl } from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import * as html2canvas from 'html2canvas';
import { ToastrService } from 'ngx-toastr';
// import { Select2OptionData } from 'ng2Select2';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { ChatService } from 'src/app/services/chat.service';
import { Select2OptionData } from 'ng-select2';
declare var $: any;
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { title } from 'process';


@Component({
  selector: 'app-main-exhibitors',
  templateUrl: './main-exhibitors.component.html',
  styleUrls: ['./main-exhibitors.component.scss']
})
export class MainExhibitorsComponent implements OnInit {
  public exampleData: Array<Select2OptionData>;
  @ViewChild('position') position: ElementRef;

  liveMsg = false;
  standardList: any = [];
  premiumList: any = [];
  hell: any = [];
  value:any=[];
  vell: any = [];
  finalArray:any =[];
  dataExploded: any = [];
  shell:any;
  well:any;
  cat:any = [];
  countryList:any=[];
  stateList:any=[];
  hello:any
  classed:any
  classed2:any
  Exhibitor :any = 'Exhibitors Search';
  //  exampleData: any = [];
  red: any = 'Select';
  constructor(private router: Router, private chat: ChatService, private _fd: FetchDataService) { }
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  ngOnInit(): void {
    
    this.dropdownList = [
      { "id": 1, "itemName": "Countries Pavilion" },
      { "id": 2, "itemName": "States Pavilion" },
      { "id": 3, "itemName": "BIRAC Pavilion" },
      { "id": 4, "itemName": "Incubator Pavilion" },
      { "id": 5, "itemName": "Exhibitors Pavilion" },
      { "id": 6, "itemName": "Annual Partners Pavilion" },
      { "id": 7, "itemName": "STPI Pavilion" }
      // { "id": 8, "itemName": "Energy Effiency" },
      // { "id": 9, "itemName": "New/Alternate Energy(Hydrogen,Geothermal,Tidal,etc)" },
      // { "id": 10, "itemName" : "Hybrid RE"},
      // { "id": 11, "itemName":"Wind/Offshore wind"}
    ];
    this.countryList = [
      {"id":64, title:'Germany', routes:'/exhibitionHall/germany'},
      {"id":64, title:'France', routes:'/exhibitionHall/france'},
      {"id":205, title:'Denmark', routes:'/exhibitionHall/denmark'},
      {"id":64, title:'European Union', routes:'/exhibitionHall/EUmain'},
      {"id":205, title:'European-Wind', routes:'/exhibitionHall/EUwind'},
      {"id":64, title:'European-Solar', routes:'/exhibitionHall/EUsolar'},
      {"id":234, title:'European-Avere',routes:'/exhibitionHall/EUavere'},
      {"id":64, title:'Australia', routes:'/exhibitionHall/australia'},
      {"id":64, title:'UK', routes:'/exhibitionHall/uk'},
     
     
    
       
      // {"id":64, title:'Germany', routes:''}
    ];
    this.stateList = [
      {"id":64, title:'Himachal Pradesh', routes:'/exhibitionHall/himachal'},
      {"id":64, title:'Rajasthan', routes:'/exhibitionHall/rajsthan'},
      {"id":64, title:'Tamil Nadu', routes:'/exhibitionHall/tamlinadu'},
       {"id":64, title:'Madhya Pradesh', routes:'/exhibitionHall/madhyapradesh'},
       {"id":64, title:'Gujarat', routes:'/exhibitionHall/gujrat'},
    ];
    this.selectedItems = [
      // { "id": 2, "itemName": "Power" },
      // { "id": 3, "itemName": "Energy Effiency" },
      // { "id": 4, "itemName": "Bio Energy" },
      // { "id": 5, "itemName": "Solar" }
    ];
    this.dropdownSettings = {
      singleSelection: false,
      text: "Select Exhibition Category",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
    this.changeRoute();
    // this.changeSelect();
    $("#position").select2({
      allowClear: true,
      placeholder: 'Position'
    });
    $('#position').on('change', function () {
      var data = $("#position option:selected").text();
      $("#test").val(data);
    })
    // this.exampleData = [
    //   {
    //     id: 'solar',
    //     text: 'Solar'
    //   },
    //   {
    //     id: 'wind',
    //     disabled: true,
    //     text: 'Wind/ Offshore Wind'
    //   },
    //   {
    //     id: 'bio',
    //     text: 'Bio - Energy '
    //   },
    //   {
    //     id: 'hydro',
    //     text: 'Hydro / SHP'
    //   }

    //   ];
    // this.search();
    this.standardStalls();
    this.premiumStalls();
    // this.chat.getconnect('toujeo-52');
    // this.chat.getMessages().subscribe((data=>{
    //   console.log('data',data);
    //   if(data == 'start_live'){
    //     this.liveMsg = true;
    //   }
    //   if(data == 'stop_live'){
    //     this.liveMsg = false;
    //   }

    // }));
  }
  onItemSelect(item: any) {
    this.value=[];
    this.cat.push(item)
    // let value =[];
    this.vell.map(main=>{
      this.dataExploded = main.category.split('|');
      console.log(this.dataExploded);
      
      this.dataExploded.map(data=>{
        this.cat.map(selectedItem=>{
        if(selectedItem.itemName == data){
          console.log(selectedItem.itemName)
          if(this.value.findIndex(data=>data.text==main.text)==-1){
            this.value.push(main)
          }
          // value.push(main)
        }
      })
      })
    })
     this.exampleData = this.value
    // this.exampleData = this.vell.filter(data=>data.category==item.itemName)
  }
  OnItemDeSelect(item: any) {
    this.value=[];
    console.log(item);
    console.log(this.vell);
    
    this.cat.splice(this.cat.findIndex(data=>data.itemName==item.itemName),1)
    this.vell.map(main=>{
      this.dataExploded = main.category.split('|');
      console.log(this.dataExploded);
      
      this.dataExploded.map(data=>{
        this.cat.map(selectedItem=>{
        if(selectedItem.itemName == data){
          console.log(selectedItem.itemName)
          if(this.value.findIndex(data=>data.text==main.text)==-1){
            this.value.push(main)
          }
          // value.push(main)
        }
      })
      })
    })
    if(this.value.length){
      this.exampleData = this.value
    }else{
      this.exampleData = this.vell
    }

    console.log(this.selectedItems);
  }
  onSelectAll(items: any) {
    this.value=[]
    this.cat.push(items)
    console.log(items);
   this.exampleData = this.vell
  }
  onDeSelectAll(items: any) {
    this.value=[]
    console.log(items);
    this.cat=[];
    this.exampleData = this.vell
  }

  SearchedKeyword = '';
  alertValue(e) {
    this.SearchedKeyword = e;
  }
  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    if(virtual.company == ''){
      virtual.company = 'others';
    };
    const formData = new FormData();
    formData.append('event_id', virtual.event_id);
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', virtual.company);
    formData.append('designation', virtual.designation);
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }
  lifeboy(title) {
    this.router.navigate(['/exhibitionHall/lifeboy', title]);
  }
  premium(title) {
    this.router.navigate(['/exhibitionHall/premium', title]);
  }
  state(title) {
    this.router.navigate(['/exhibitionHall/state', title]);
  }
  country(title) {
    this.router.navigate(['/exhibitionHall/country', title]);
  }
  adapt() {
    this.router.navigate(['/exhibitionHall/adapt']);
  }
  arise() {
    this.router.navigate(['/exhibitionHall/arise']);
  }
  achive() {
    this.router.navigate(['/exhibitionHall/achive']);
  }
  closeStandard() {
    $('.exDirectory').modal('hide');
    $('.countryDir').modal('hide');
    $('.stateDir').modal('hide');
    $('.stpiDir').modal('hide');
    $('.biracDir').modal('hide');
    $('.incubatorDir').modal('hide');
    $('.annualDir').modal('hide');
    
  }
  closePremium() {
    $('.premDirectory').modal('hide');
  }
  closeStates() {
    $('.stateDirectory').modal('hide');
  }
  standardStalls() {
    this._fd.getstandarad().subscribe((res: any) => {
      // console.log(res);
      this.standardList = res.result;
    });
  }
  premiumStalls() {
    this._fd.getpremium().subscribe((res: any) => {
      this.premiumList = res.result;
    });
  }
 

  search() {
    // alert(this.red);
     this.hello =  this.vell.find(data=>data.id==this.red)
    console.log(this.hello);
    this.classed = this.hello.allCat;
    this.classed2 = this.hello.text;
    // alert(this.red)
    if(this.classed == 'standard'){
      this.router.navigate(['/exhibitionHall/lifeboy/' + this.red])
    }
    if(this.classed == 'premium'){
      this.router.navigate(['/exhibitionHall/' + this.classed + '/' + this.red])
    }
    if (this.classed2 == 'GERMANY') {
      this.router.navigate(['/exhibitionHall/germany'])
    }
    if (this.classed2 == 'FRANCE') {
      this.router.navigate(['/exhibitionHall/france'])
    }
    if (this.classed2 == 'DENMARK') {
      this.router.navigate(['/exhibitionHall/denmark'])
    }
    if (this.classed2 == 'EUROPEON UNION') {
      this.router.navigate(['/exhibitionHall/EUmain'])
    }
    if (this.classed2 == 'EUROPEON-WIND') {
      this.router.navigate(['/exhibitionHall/EUwind'])
    }
    if (this.classed2 == 'EUROPEON-SOLAR') {
      this.router.navigate(['/exhibitionHall/EUsolar'])
    }
    if (this.classed2 == 'EUROPEON-AWERE') {
      this.router.navigate(['/exhibitionHall/EUavere'])
    }
     
    if (this.classed2 == 'AUSTRALIA') {
      this.router.navigate(['/exhibitionHall/australia'])
    }
    if (this.classed2 == 'UK') {
      this.router.navigate(['/exhibitionHall/uk'])
    }
    if (this.classed2 == 'HIMACHAL') {
      this.router.navigate(['/exhibitionHall/himachal'])
    }
    if (this.classed2 == 'HIMACHAL PRADESH') {
      this.router.navigate(['/exhibitionHall/himachal'])
    }
    if (this.classed2 == 'RAJASTHAN') {
      this.router.navigate(['/exhibitionHall/rajsthan'])
    }
    if (this.classed2 == 'TAMIL NADU') {
      this.router.navigate(['/exhibitionHall/tamlinadu'])
    }
    // if (this.classed2 == 'HIMACHAL PRADESH') {
    //   this.router.navigate(['/exhibitionHall/madhyapradesh'])
    // }
    // if (this.classed2 == 'HIMACHAL PRADESH') {
    //   this.router.navigate(['/exhibitionHall/gujrat'])
    // }
  }

  searchMulti(){
    alert(this.selectedItems);
    // if (this.selectedItems != 'Select') {
    //   this.router.navigate(['/exhibitionHall/lifeboy/' + this.red])
    // }
  }
  
  // heee() {
  //   alert("ff");
  // }
  changeRoute() {
    this._fd.ExhiSearch().subscribe((res: any) => {
      this.hell = res.result;
      console.log(this.hell)
      this.vell = this.hell.map(({ id, title,category, core_sectors }) => {
        let csvdata = {
          text: title,
          id: id,
          category : core_sectors,
          allCat :category,
        }
        return csvdata
      });
      console.log(this.vell)
      //  this.cat =this.vell
      this.exampleData = this.vell
      // console.log(this.allCat + 'fdsfdsfds')

    });
  }

  // changeSelect() {
  //   this._fd.ExhiSearch().subscribe((res: any) => {
  //     this.hell = res.result;
  //     console.log(this.hell)
  //     this.shell = this.hell.map(({ id, core_sectors }) => {
  //       let csvdata = {
  //         itemName: core_sectors,
  //         id: id,
  //       }
  //       return csvdata
  //     });
  //     console.log(this.shell)
  //     this.dropdownList = this.shell
  //     console.log(this.dropdownList + 'fdsfdsfds')

  //   });
  // }
  
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.closeStandard();
    this.closePremium();
    this.closeStates();
  }
}
