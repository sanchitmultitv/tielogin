import { Component, OnInit, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {FormControl} from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import  * as html2canvas  from 'html2canvas';
import {ToastrService} from 'ngx-toastr';

import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;
@Component({
  selector: 'app-germany',
  templateUrl: './germany.component.html',
  styleUrls: ['./germany.component.scss']
})
export class GermanyComponent implements OnInit {
  textMessage = new FormControl('');
  msg;
  qaList;
  interval;
  product;
  pord_desc;
breif_flag = false;
exhibition_id = '0';
brouchers: any = [];
graphic:any;
  banner: any;
  exhibitionName ='64'; 
  exhibiton:any=[];
  documents:any=[];
  videos:any=[];
  videoSource;
  newTimeSlots:any=[];
timeVal;
boolTime= false;
isShow = true;
model: NgbDateStruct;
date: {year: number, month: number};
newMessage: string;
  msgs: string;
  newMSg =[];
  showDiv = true;
  showchat = true;
  hidechat = false;
  messageList = [];
  roomName: string;
  salesPerson:any=[];
  myId;
  uName;
  myid;
  @ViewChild('screen') screen: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('downloadLink') downloadLink: ElementRef;
  constructor(private _fd:FetchDataService, private route: ActivatedRoute, private toastr: ToastrService,private calendar: NgbCalendar, private renderer: Renderer2, private chat: ChatService) { }

  ngOnInit(): void {
  this.getExhibitionData();
  let virtual: any = JSON.parse(localStorage.getItem('virtual'));
  this.myid = virtual.id;
  let yyyy: any = new Date().getFullYear();
  let dd: any = new Date().getDate();
  let mm: any = new Date().getMonth() + 1;
  let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
  let action = 'click_exhibition_GERMANY';
  if (dd < 10) {
    dd = '0' + dd;
  }
  if (mm < 10) {
    mm = '0' + mm;
  }
  if(virtual.company == ''){
    virtual.company = 'others';
  };
  const formData = new FormData();
  formData.append('event_id', virtual.event_id);
  formData.append('user_id', virtual.id);
  formData.append('name', virtual.name);
  formData.append('email', virtual.email);
  formData.append('company', virtual.company);
  formData.append('designation', virtual.designation);
  formData.append('action', action);
  formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
  this._fd.analyticsPost(formData).subscribe(res => {
    console.log('asdf', res);
  });
  this.getQA();
  this.chat.getconnect('toujeo-57');
  this.chat.getMessages().subscribe((data => {
    console.log('socketdata', data);

    let check = data.split('_');
     if(check[0] == 'one2one' && check[1]== this.myid) {
      //alert(data);
       this.getQA();
     }
  }));
  this.banner = 'assets/reinvest/Country_Stall_View/Germany_stallview.jpg';
  this.documents = [
    {title:'IGEF Newsletter October', pdf:'https://virtualapi.multitvsolution.com/countryStall/germany/pdf/IGEF_Newsletter_October.pdf'},
    {title:'IGEF Newsletter July', pdf:'https://virtualapi.multitvsolution.com/countryStall/germany/pdf/IGEF_Newsletter_July.pdf'},
    {title:'Repowering of old wind turbines in India', pdf:'https://virtualapi.multitvsolution.com/countryStall/germany/pdf/IGEF_Repowering_Wind.pdf'},
    {title:'Impact of COVID-19 on Renewable Energy Market in India', pdf:'https://virtualapi.multitvsolution.com/countryStall/germany/pdf/Impact_Covid19_RE_IND.pdf'},
    {title:'Flexibility of thermal power plants in India', pdf:'https://virtualapi.multitvsolution.com/countryStall/germany/pdf/Flexibility_power_plant_IND.pdf'},
    {title:'Renewable Energy Market Overview India', pdf:'https://virtualapi.multitvsolution.com/countryStall/germany/pdf/RE_Market_Overview_IND.pdf'},
    {title:'COBENEFITS India Policy Report', pdf:'https://virtualapi.multitvsolution.com/countryStall/germany/pdf/COBENEFITS-India_Policy-Report.pdf'},
    {title:'Solar Payback – Solar Heat for Industry India', pdf:'https://virtualapi.multitvsolution.com/countryStall/germany/pdf/Solar_Payback_SolarHEAT.pdf'},
    {title:'Wind Energy Digital 2020', pdf:'https://virtualapi.multitvsolution.com/countryStall/germany/pdf/Wind_Energy_Digital_2020.pdf'},
    {title:'IGCC Magazine - Indo-German Economy - IGE 5', pdf:'https://virtualapi.multitvsolution.com/countryStall/germany/pdf/IGCC_Magazine-Indo-German_Economy-IGE5.pdf'},
    {title:'IGCC Newsletter November 2020', pdf:'https://virtualapi.multitvsolution.com/countryStall/germany/pdf/IGCC_Newslette_November_2020.pdf'},
    {title:'IGCC Service Catalogue', pdf:'https://virtualapi.multitvsolution.com/countryStall/germany/pdf/IGCC_Service_Catalogue.pdf'},
    {title:'IGCC - Senior Experten Service', pdf:'https://virtualapi.multitvsolution.com/countryStall/germany/pdf/IGCC-Senior_Experten_Service.pdf'},
    {title:'Fraunhofer India Brochure', pdf:'https://virtualapi.multitvsolution.com/countryStall/germany/pdf/Fraunhofer_India_Brochure.pdf'},
    // tslint:disable-next-line: max-line-length
    {title:'Fraunhofer ISE – A short Overview', pdf:'https://virtualapi.multitvsolution.com/countryStall/germany/pdf/Fraunhofer_ISE-AShort_Overview.pdf'},
    {title:'Fraunhofer Agri-PV', pdf:'https://virtualapi.multitvsolution.com/countryStall/germany/pdf/Fraunhofer_Agri_PV.PDF'}
  ];
  this.videos = [
    {title:'Business opportunities for AgroPV in India', videos:'https://www.youtube.com/watch?v=qP2KQwPeT8U&list=PLYmsNo201FNU-RQ0MAKwDC9rO5rhA_a87'},
    {title:'Renewable Energy Power Supply for C&I Consumers', videos:'https://www.youtube.com/watch?v=F2u_nJvJPZQ&list=PLYmsNo201FNXjp9CW8QYj3SyU65WDrXZM'},
    {title:'Self-consumption of Renewable Power', videos:'https://www.youtube.com/watch?v=hn_cABJX6VA&list=PLYmsNo201FNUjGxiaP1PQ85j9AyUQemkv'},
    {title:'PV Rooftop & Storage', videos:'https://www.youtube.com/watch?v=ONG1da1JJdk&list=PLYmsNo201FNXlPy1D8-8mHDsalyfWQQvY'},
    {title:'COBENEFITS of Renewables in India', videos:'https://www.youtube.com/watch?v=tLA_DIcfUZA'},
    {title:'COBENEFITS of Climate Action with RE', videos:'https://www.youtube.com/watch?v=WDhcw52E91Q&feature=youtu.be'},
    {title:'Wind Energy Hamburg - The global on & offshore event', videos:'https://youtu.be/2O-N68tsPF8'},
    {title:'IGCC - Live without boundaries', videos:'https://www.youtube.com/watch?v=YDYs9Or98Uo&t=1s'},
  ];
  this.salesPerson = [
  {name:'Madhuri Negi', position:'Indo-German Energy Forum (IGEF) SO', email:'Communications@energyforum.in', contact:'9899045698'},
  {name:'Shivani Chaturvedi', position:'Indo-German Chamber of Commerce', email:'shivani@indo-german.com ', contact:'9818171692'}
  ];
  }
  getExhibitionData(){
    let data = JSON.parse(localStorage.getItem('virtual'));
    this._fd.getExhibition(this.exhibitionName, 'country').subscribe(res => {
        this.exhibiton = res.result;
        console.log(this.banner, 'vauvduyvuduv')
      });
    // this._fd.getBrouchers(this.exhibitionName,data.id).subscribe(res=> {
    //     console.log(res.result);
    //     this.brouchers = res.result;
    //   })
      
  
   
  }
  download(i, name){
    // html2canvas(this.screen.nativeElement).then(canvas => {
    //   this.canvas.nativeElement.src = canvas.toDataURL();
    //   this.downloadLink.nativeElement.href = canvas.toDataURL('image/png');
    //   this.downloadLink.nativeElement.download = name + '.png';
    //   this.downloadLink.nativeElement.click();
      
    // });
    // this.toastr.success( 'Card Dropped Succesfully!');
    var container = document.getElementById("hello" + i); //specific element on page
  //var container = document.body; // full page 
  // tslint:disable-next-line: indent
  // tslint:disable-next-line: align
  html2canvas(container).then(function(canvas) {
            var link = document.createElement("a");
            document.body.appendChild(link);
            link.download = name + ".png";
            link.href = canvas.toDataURL('image/png');
            link.target = '_blank';
            link.click();
        });
       // this.toastr.success( 'Card Dropped Succesfully!');
  }
  playShowVideo(video) {
    this.videoSource = video;
    let playVideo: any = document.getElementById("video");
    playVideo.play();
    $('#playVideo').modal('show');
  }
  
  closeModalVideo(){
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }
  
  closeCall(val){
 $(val).modal('hide');
  }
  sendMessage() {
    this.roomName = 'ddbjb1bk';
    if (this.msgs != '') {
      this.chat.sendMessage(this.msgs, this.uName, this.roomName);
      this.msgs = '';
    }
  }
  hidediv() {
    this.showDiv = false;
    this.showchat = false;
    this.hidechat = true;
      }
      show() {
        this.showDiv = true;
        this.hidechat = false;
    this.showchat = true;
          }
          getUser() {
            let user = JSON.parse(localStorage.getItem('virtual'));
            this.uName = user.name;
          //  console.log(this.uName);
           // let room = JSON.parse(localStorage.getItem('room_id'));
            //console.log(room);
            this.roomName = 'ddbjb1bk';
            this.chat.addUser(this.uName, this.roomName);
          }
      getQA(){
          //this.exhibitionName = '';
        //  console.log('exhibitonid',this.exhibition_id);
          let data = JSON.parse(localStorage.getItem('virtual'));
         // console.log('uid',data.id);
          this._fd.getanswers(data.id,this.exhibitionName).subscribe((res=>{
            //console.log(res);
            this.qaList = res.result;
            // alert('hello');
          }))
      
        }
        postQuestion(value) {
           // this.exhibitionName ='';
            let data = JSON.parse(localStorage.getItem('virtual'));
         console.log('value',value);
        // this.getQA();
        if(value != undefined){
            this._fd.askQuestions(data.id,data.name, value,this.exhibitionName,'Germany Stall').subscribe((res=>{
            if(res.code == 1) {
              this.msg = 'Submitted Succesfully';
            // var d = $('.chat_message');
            // d.scrollTop(d.prop("scrollHeight"))
            }
            this.getQA();
             
            setTimeout(() => {
              $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight-100;
              this.msg = '';
      //$('.liveQuestionModal').modal('hide');
            }, 2000);
            // setTimeout(() => {
            //   this.msg = '';
            //   $('.liveQuestionModal').modal('hide');
            // }, 2000);
            this.textMessage.reset();
          }))
        }
        
      
        }
        changeProdcut(prod,descs){
          this.product = prod;
          this.pord_desc =descs;
        }
        closeproduct(){
          $('.productModal').modal('hide');
        }
        closeChat(){
          $('.liveQuestionModal').modal('hide');
        }
        closeright(){
          $('.rightGraphcModal').modal('hide');
        }
        closeLeft(){
          $('.leftGraphicModal').modal('hide');
        }
}
