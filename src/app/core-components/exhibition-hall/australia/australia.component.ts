import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-australia',
  templateUrl: './australia.component.html',
  styleUrls: ['./australia.component.scss']
})
export class AustraliaComponent implements OnInit {
  banner;
  constructor(private _fd: FetchDataService) { }
  
  ngOnInit(): void {
    this.banner = 'assets/reinvest/Country_Stall_View/Aus-Stall_view.jpg';
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    let action = 'click_exhibition_AUSTRALIA';
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    if(virtual.company == ''){
      virtual.company = 'others';
    };
    const formData = new FormData();
    formData.append('event_id', virtual.event_id);
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', virtual.company);
    formData.append('designation', virtual.designation);
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }

}
