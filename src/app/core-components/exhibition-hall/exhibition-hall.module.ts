import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExhibitionHallRoutingModule } from './exhibition-hall-routing.module';
import { ExhibitLifeComponent } from './exhibit-life/exhibit-life.component';
import { LifeboyComponent } from './lifeboy/lifeboy.component';
import { VgCoreModule } from 'videogular2/compiled/core';
import { VgControlsModule } from 'videogular2/compiled/controls';
import { VgOverlayPlayModule } from 'videogular2/compiled/overlay-play';
import { VgBufferingModule} from 'videogular2/compiled/buffering';
import { AdaptComponent } from './adapt/adapt.component';
import { AriseComponent } from './arise/arise.component';
import { AchiveComponent } from './achive/achive.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { Exhibition2Component } from './exhibition2/exhibition2.component';
import { Exhibiton3Component } from './exhibiton3/exhibiton3.component';
import { PremiumComponent } from './premium/premium.component';
import { CountryComponent } from './country/country.component';
import { StateComponent } from './state/state.component';
import { MainExhibitorsComponent } from './main-exhibitors/main-exhibitors.component'
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgSelect2Module } from 'ng-select2';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { LSelect2Module } from 'ngx-select2';
import { GermanyComponent } from './germany/germany.component';
import { UkComponent } from './uk/uk.component';
import { FranceComponent } from './france/france.component';
import { DenmarkComponent } from './denmark/denmark.component';
import { AustraliaComponent } from './australia/australia.component';
import { HimachalComponent } from './himachal/himachal.component';
import { RajshtanComponent } from './rajshtan/rajshtan.component';
import { TamilnaduComponent } from './tamilnadu/tamilnadu.component';
import { MadhyapradeshComponent } from './madhyapradesh/madhyapradesh.component';
import { GujratComponent } from './gujrat/gujrat.component';
import { EuropeComponent } from './europe/europe.component';
import { WindeuropeComponent } from './windeurope/windeurope.component';
import { SolareuropeComponent } from './solareurope/solareurope.component';
import { AvereeuropeComponent } from './avereeurope/avereeurope.component';
@NgModule({
  declarations: [ExhibitLifeComponent, LifeboyComponent, AdaptComponent, AriseComponent, AchiveComponent, Exhibition2Component, Exhibiton3Component, PremiumComponent, CountryComponent, StateComponent, MainExhibitorsComponent, GermanyComponent, UkComponent, FranceComponent, DenmarkComponent, AustraliaComponent, HimachalComponent, RajshtanComponent, TamilnaduComponent, MadhyapradeshComponent, GujratComponent, EuropeComponent, WindeuropeComponent, SolareuropeComponent, AvereeuropeComponent],
  imports: [
    NgMultiSelectDropDownModule.forRoot(),
    NgSelect2Module,
    NgSelectModule,
    CommonModule,
    ExhibitionHallRoutingModule,
    VgCoreModule,
    VgControlsModule,
    VgBufferingModule,
    VgOverlayPlayModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    LSelect2Module,
    AngularMultiSelectModule
  ]
})
export class ExhibitionHallModule { }
