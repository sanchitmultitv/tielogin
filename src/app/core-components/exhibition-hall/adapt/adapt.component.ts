import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MessageService } from 'src/app/services/message.service';
import { chat } from "src/app/core-components/exhibition-hall/chat.js";
import { CometChat } from "@cometchat-pro/chat";


declare var $:any;
@Component({
  selector: 'app-adapt',
  templateUrl: './adapt.component.html',
  styleUrls: ['./adapt.component.scss']
})
export class AdaptComponent implements OnInit {

  exhibiton:any=[];
  documents:any=[];
  textMessage = new FormControl('');
  msg;
  qaList;
  exhibition_id;
  pdf;
  document;
  documentName;
  constructor(private _fd: FetchDataService, private chat: ChatService, private sanitiser: DomSanitizer, private messageService: MessageService) { }

  ngOnInit(): void {
    this.getExhibit();
    setTimeout(() => {
      this.getQA();
    }, 1000);
    this.chat.getconnect('toujeo-52');
    this.chat.getMessages().subscribe((data=>{
      console.log('socketdata', data);
      if(data == 'question_reply'){
        this.getQA();
      }
    }));
  }
  getDocument(prof){
    $('.pdfModal').modal('show');
    // window.open("https://s3.ap-south-1.amazonaws.com/acma.multitvsolution.in/assets/acme/speakers_profiles/Kenichi Ayukawa/Brief Profile - Mr. Kenichi Ayukawa.pdf","viewer"); 
    this.document=this.sanitiser.bypassSecurityTrustResourceUrl(prof);
    this.documentName = 'Adapt Brochure';
    this.messageService.sendMessage(this.document);
    console.log(prof,'fulldocss');
    console.log(this.document,'docs');
  }
  closepdf(){
    $('.pdfModal').modal('hide');
  }
  getExhibit(){
    this._fd.getExhibition('Adapt','hh').subscribe(res=>{
      
      this.exhibiton = res.result[0];
      this.documents = res.result[0].document;
      this.exhibition_id = res.result[0].id;
      console.log('exhibition',this.exhibiton); 
      //localStorage.setItem('exhibitData',JSON.stringify(res.result));
    });
  }
  getQA(){
    console.log('exhibitonid',this.exhibition_id);
    let data = JSON.parse(localStorage.getItem('virtual'));
   // console.log('uid',data.id);
    this._fd.getanswers(data.id,this.exhibition_id).subscribe((res=>{
      //console.log(res);
      this.qaList = res.result;
    }))
  }

  lightbox_open() {
    //this.videoUrl = video;
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    window.scrollTo(0, 0);
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    lightBoxVideo.play();

  }
  lightbox_close() {
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    document.getElementById('light').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
    lightBoxVideo.pause();
  }
  postQuestion(value){
    let data = JSON.parse(localStorage.getItem('virtual'));
  //  console.log(value, data.id);
    this._fd.askQuestions(data.id,value,this.exhibition_id,'d','d').subscribe((res=>{
      //console.log(res);
      if(res.code == 1){
        this.msg = 'Submitted Succesfully';
        this.getQA();
      }
      setTimeout(() => {
        this.msg = '';
        $('.liveQuestionModal').modal('hide');
      }, 2000);
      this.textMessage.reset();
    }))
  }
  closePopupdocs(){
$('.docsModal').modal('hide');
  }
  closePopup(){
    $('.salesModal').modal('hide');
      }
      openCallBox(){
        $('.scheduleCallmodal').modal('show');
      }
    closeChat(){
        $('.liveQuestionModal').modal('hide');
      }

}
