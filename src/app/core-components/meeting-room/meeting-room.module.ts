import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MeetingRoomRoutingModule } from './meeting-room-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MeetingRoomRoutingModule
  ]
})
export class MeetingRoomModule { }
