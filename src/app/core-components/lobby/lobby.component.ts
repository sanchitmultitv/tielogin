import { title } from 'process';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
declare var introJs: any;
declare var $: any;
import * as Clappr from 'clappr';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';
import { sharedContent } from './shared';
declare var twttr: any;
declare var FB: any;

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})

export class LobbyComponent implements OnInit, AfterViewInit {
  videoEnd = false;
  receptionEnd = false;
  showVideo = false;
  auditoriumLeft = false;
  auditoriumRight = false;
  exhibitionHall = false;
  registrationDesk = false;
  networkingLounge = false;
  intro: any;
  player: any;
  actives: any = [];
  liveMsg = false;
  videoSOurce;
  redirect;
  sharedContent=sharedContent;
  slideContent=[];
  innerWidth:any;
  phonex = false;
  infinty = false;
  quantum = false;
  lounge = false;
  pitch = false;
  videoUrl;
  documents= [{title:'Flipkart Brochure', pdf:'assets/tie/pdfs/Brochures/Flipkart_Brochure.pdf'},
  {title:'GreytHR Brochure..', pdf:'assets/tie/pdfs/Brochures/GreytHR_Brochure...pdf'},
  {title:'Hughes Systique Brochure', pdf:'assets/tie/pdfs/Brochures/Hughes_Systique_Brochure.pdf'},
  {title:'Sequoia-Surge Brochure', pdf:'assets/tie/pdfs/Brochures/Sequoia-Surge.pdf'},
  {title:'TARGET Brochure', pdf:'assets/tie/pdfs/Brochures/TARGET_Brochure.pdf'},
  {title:'Westland - Parmesh Brochure', pdf:'assets/tie/pdfs/Brochures/Westland-Parmesh.pdf'},
   {title:'WFH Images', pdf:'assets/tie/pdfs/Brochures/WFH_Images.pdf'}
  ];
  videos = [{title:'1. India Inspires India _ #GoogleForIndia', video:'assets/tie/videos/Sponsor Videos/1.IndiaInspiresIndia.mp4'},
  {title:'2. Indie Games Accelerator journey _ Octathorpe web consultants (Android Developer Story)', video:'assets/tie/videos/Sponsor Videos/2.IndieGamesAccelerator.mp4'},
  {title:'3. The story of Nutan and her daughters _ Read Along', video:'assets/tie/videos/Sponsor Videos/3.ThestoryofNutan.mp4'},
  {title:'4. Visakhapatnam G4e Revised Video With Subtitles', video:'assets/tie/videos/Sponsor Videos/4.Visakhapatnam.mp4'},
  {title:'5. Introducing Sequoia Capital India’s Surge', video:'assets/tie/videos/Sponsor Videos/5.SurgeMarketing.mp4'},
  {title:'6. Flipkart Video 1', video:'assets/tie/videos/Sponsor Videos/6.FlipkartVideo_1.mp4'},
  {title:'7. Flipkart Video 2', video:'assets/tie/videos/Sponsor Videos/7.FlipkartVideo_2.mp4'},
  {title:'8. WDC Video', video:'assets/tie/videos/Sponsor Videos/8.WDCVideo.mp4'},
  {title:'9. Target Video', video:'assets/tie/videos/Sponsor Videos/9.TargetVideo.mp4'},
  {title:'10. MG Motor Video', video:'assets/tie/videos/Sponsor Videos/10.MgMotorVideo.mp4'},
  {title:'11. GreytHR Generic Brand Video', video:'assets/tie/videos/Sponsor Videos/11.GreytHRGeneric.mp4'},
  ];
  // videoUrl;
  @ViewChild('recepVideo', { static: true }) recepVideo: ElementRef;

  constructor(private router: Router, private _fd: FetchDataService, private chat: ChatService) { }
  // videoPlayer = 'https://s3.ap-south-1.amazonaws.com/acma.multitvsolution.in/assets/acme/acma-home_1.mp4';
  ngOnInit(): void {
    this.innerWidth=window.innerWidth;
    twttr.widgets.load();
    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    // this.getUserguide();
    this.slideContent=this.sharedContent;
    this.sharedContent.forEach((ele:any)=>{
      this.slideContent.push({img:ele.img,wd:ele.wd});
    });
    this.stepUpAnalytics('Lobby');
    // this.audiActive();
    this.chat.getconnect('toujeo-139');
    this.chat.getMessages().subscribe((data => {
        console.log('data',data);
        let notify = data.split('_');
        if(notify[0] == 'start' && notify[1] == 'live'){
      this.liveMsg = true;
      this.redirect = notify[2];
     }
        if (data == 'stop_live') {
        this.liveMsg = false;
      }

    }));
    let playVideo: any = document.getElementById('playVideo');
    window.onclick = (event) => {
      if (event.target == playVideo) {
        playVideo.style.display = "none";
        let pauseVideo: any = document.getElementById("video");
        pauseVideo.currentTime = 0;
        pauseVideo.pause();
      }
    }
  }

  ngAfterViewInit() {
    this.playAudio();
  }
  closePopup() {
    $('.yvideo').modal('hide');
  }
  closePopupspon(){
    $('.sponsorModal').modal('hide');
  }
  closePopupbrouch(){
    $('.brouchersModal').modal('hide');
  }
  closePopupspeak(){
    $('.speakerModal').modal('hide');
  }
  getvideo(video){
    this.videoUrl = video;
  }
  // audiActive() {
  //   this._fd.activeAudi().subscribe(res => {
  //     this.actives = res.result;
  //   })
  // }

  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerWidth <= 767) {
      this.player.resize({
        width: window.innerWidth / 6.69, height: window.innerHeight / 7.95
      });

    } else {
      this.player.resize({
        width: window.innerWidth / 6.69, height: window.innerHeight / 7.95
      });
    }
  }
  playAudio() {
    // localStorage.setItem('play', 'play');
    // let abc: any = document.getElementById('myAudio');
    // abc.play();
    // alert('after login audio')
  }
  playevent() {
    this.videoEnd = true;
  }
  playReception() {
    this.router.navigate(['/welcome']);
    // this.receptionEnd = true;
    // this.showVideo = true;
    // let vid: any = document.getElementById("recepVideo");
    // vid.play();

  }
  receptionEndVideo() {
    this.router.navigate(['/welcome']);

  }
  gotoAuditoriumFront() {

    this.registrationDesk = true;
    this.showVideo = true;
    let vid: any = document.getElementById("regDeskvideo");
    vid.play();

  }
  playAudiThreeFourEndVideo() {
    this.router.navigate(['exhibitionHall']);

  }

  // gotoAuditoriumFront() {
  //   this.router.navigate(['/auditorium/front-desk']);
  // }

  playAuditoriumLeft() {
    if (this.actives[3].status == true) {
      this.auditoriumLeft = true;
      this.showVideo = true;
      let vid: any = document.getElementById("audLeftvideo");
      vid.play();
    }
    else {
      $('.audiModal').modal('show');
    }

  }
  gotoConferenceHall() {
    this.router.navigate(['/auditorium/one']);
  }
  gotoExhibition(name) {
    if (name === 'one') {
      this.router.navigate(['/exhibitionHall']);
    }
    if (name === 'two') {
      this.router.navigate(['/exhibitionHall/exhibition2']);
    }
    if (name === 'three') {
      this.router.navigate(['/exhibitionHall/exhibition3']);
    }
  }
  gotoAuditoriumLeftOnVideoEnd() {
    this.router.navigate(['/auditorium/left']);
  }
  playAuditoriumRight() {
    if (this.actives[2].status == true) {
      this.auditoriumRight = true;
      this.showVideo = true;
      let vid: any = document.getElementById("audRightvideo");
      vid.play();
    }
    else {
      $('.audiModal').modal('show');
    }
  }
  gotoAuditoriumRightOnVideoEnd() {
    this.router.navigate(['/auditorium/right']);
  }
  playExhibitionHall() {
    this.exhibitionHall = true;
    this.showVideo = true;
    let vid: any = document.getElementById("exhibitvideo");
    vid.play();
  }
  playPhonex() {
    this.phonex = true;
    this.showVideo = true;
    let vid: any = document.getElementById("phonexvideo");
    vid.play();
  }
  playInfinty(){
    this.infinty = true;
    this.showVideo = true;
    let vid: any = document.getElementById("infintyvideo");
    vid.play();
  }
  playQunatum(){
    this.quantum = true;
    this.showVideo = true;
    let vid: any = document.getElementById("quantumvideo");
    vid.play();
  }
  playpitch(){
    this.pitch = true;
    this.showVideo = true;
    let vid: any = document.getElementById("pitchvideo");
    vid.play();
  }
  playlounge(){
    this.lounge = true;
    this.showVideo = true;
    let vid: any = document.getElementById("loungevideo");
    vid.play();
  }
  gotoloungemend(){
    this.router.navigate(['/tech-twelve']);
  }
  gotopitchend(){
    this.router.navigate(['/tech-fifteen']);
  }
  gotoquantumend() {
    this.router.navigate(['/tech-eight']);
  }
  gotoExhibitionHallOnVideoEnd() {
    this.router.navigate(['/exhibitionHall']);
  }
  gotophonexend() {
    this.router.navigate(['/plenary-four']);
  }
  gotoinfintyend() {
    this.router.navigate(['/tech-four']);
  }
  playRegistrationDesk() {
    this.registrationDesk = true;
    this.showVideo = true;
    let vid: any = document.getElementById("regDeskvideo");
    vid.play();
  }
  gotoRegistrationDeskOnVideoEnd() {
    this.router.navigate(['/auditorium/front-desk']);
  }
  playNetworkingLounge() {
    this.networkingLounge = true;
    this.showVideo = true;
    let vid: any = document.getElementById("netLoungevideo");
    vid.play();
  }
  gotoNetworkingLoungeOnVideoEnd() {
    this.router.navigate(['/networkingLounge']);
  }
  playShowVideo(src) {
    this.videoSOurce = src;
    let playVideo: any = document.getElementById("videosp");
    let lobbyvideo: any = document.getElementById("video");
    playVideo.play();
    lobbyvideo.pause();
    $('#playVideo').modal('show');
  }

  closeModalVideo() {
    let pauseVideo: any = document.getElementById("videosp");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }

  lightbox_open() {
    //this.videoUrl = video;
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    window.scrollTo(0, 0);
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    lightBoxVideo.play();

  }
  lightbox_close() {
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    document.getElementById('light').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
    lightBoxVideo.pause();
  }
  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    const formData = new FormData();
    formData.append('event_id', virtual.event_id);
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', virtual.company);
    formData.append('designation', virtual.designation);
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }
  getUserguide() {
    if (localStorage.getItem('user_guide') === 'start') {

      this.intro = introJs().setOptions({
        hidePrev: true,
        hideNext: true,
        exitOnOverlayClick: false,
        exitOnEsc: false,
        steps: [
          {
            element: document.querySelector("#reception_pulse"),
            intro: "<div style='text-align:center'>For any queries Relating to the event please visit the Help desk.</div>"
          },
          // {
          //   element: document.querySelectorAll("#networking_pulse")[0],
          //   intro: "<div style='text-align:center'>Click a selfie, visit the wall of fame and Network at the Lounge area.</div>"
          // },
          {
            element: document.querySelectorAll("#exhibition_pulse")[0],
            intro: "<div style='text-align:center'>Do visit the stalls placed in the Exhibition halls.</div>"
          },
          {
            element: document.querySelectorAll("#exhibition_ones")[0],
            intro: "<div style='text-align:center'>You may click on exhibitor logo to visit their stall.</div>"
          },
          {
            element: document.querySelectorAll("#audLeft_pulse")[0],
            intro: "<div style='text-align:center'>Click to attend the live conference on 15th oct 11:00 am to 12:00pm.</div>"
          },
          // {
          //   element: document.querySelectorAll("#audRight_pulse")[0],
          //   intro: "<div style='text-align:center'>All the LIVE sessions will be running in the auditorium on schedule.</div>"
          // },
          // {
          //   element: document.querySelector("#frontaudi_pulse"),
          //   intro: "<div style='text-align:center'>All the LIVE sessions will be running in the auditorium on schedule.</div>"
          // },
          // {
          //   element: document.querySelectorAll("#heighlight_pulse")[0],
          //   intro: "<div style='text-align:center'>The agendas for the events running from 21st to 25th can be viewed in agenda section.</div>"
          // },
          // {
          //   element: document.querySelectorAll("#agenda_pulse")[0],
          //   intro: "<div style='text-align:center'>Please leave your valuable feedback on 25th before you leave.</div>"
          // },


        ]
      }).oncomplete(() => document.cookie = "intro-complete=true");

      let start = () => this.intro.start();
      start();
      // if (document.cookie.split(";").indexOf("intro-complete=true") < 0)
      //   window.setTimeout(start, 1000);
    } else {
      return;
    }
  }
  // ngOnDestroy() {
  //   if (localStorage.getItem('user_guide') === 'start') {
  //     let stop = () => this.intro.exit();
  //     stop();
  //   }
  //   localStorage.removeItem('user_guide');
  // }

  gotoExhibtion(id) {
    this.router.navigate(['/exhibitionHall/lifeboy', id]);
  }
}
