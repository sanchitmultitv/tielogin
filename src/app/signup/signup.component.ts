import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FetchDataService } from '../services/fetch-data.service';
import { AuthService } from '../services/auth.service';
declare var $: any;
function emailDomainValidator(control: FormControl) {
  let email = control.value;
  if (email && email.indexOf("@") != -1) {
    let [_, domain] = email.split("@");
    if (domain !== "sanofi.com" && domain !== "sanofi-india.com") {
      return {
        emailDomain: {
          parsedDomain: domain
        }
      }
    }
  }
  return null;
}
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  checked = false;
  btnDisable = false;
  checkMessage = 'Please click on agree to Register';
  // signupForm = new FormGroup({
  //   first_name: new FormControl('', [Validators.required]),
  //   email: new FormControl('', [Validators.required, Validators.email,
  //     Validators.pattern("[^ @]*@[^ @]*"),
  //     emailDomainValidator]),
  //   job_title: new FormControl('', [Validators.required]),
  // });
  signupForm = new FormGroup({
    f_name: new FormControl('', [Validators.required]),
    l_name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    mobile: new FormControl('', [Validators.required]),
    company_name: new FormControl('', [Validators.required]),
    is_founded: new FormControl('', [Validators.required]),
    designation: new FormControl('', [Validators.required]),
    city: new FormControl('', [Validators.required]),
    policy: new FormControl('', [Validators.required]),
    gender: new FormControl('', [Validators.required]),

  });
  successMsg = false;
  failedMsg = false;
  othersCategory = false;
  changeCategory = 'Visitors Category';
  showHide;
  updateCategory(evt) {
    this.changeCategory = evt;
    if (evt === 'others') {
      $('.dropdown-menu').on('click', function (e) {
        e.stopPropagation();
      });
      this.othersCategory = true;
      this.showHide = 'show';
      this.signupForm.patchValue({
        category: ''
      });
    } else {
      this.othersCategory = false;
      this.signupForm.patchValue({
        category: evt
      });
    }
    console.log('test', this.signupForm.value.category)
  }
  showCategory() {
    let category: any = document.getElementById("categoryRadio");
    if (category.style.display === "none") {
      category.style.transition = ".5s";
      setTimeout(() => {
        category.style.display = "block";
      }, 500);
    } else {
      category.style.transition = ".5s";
      setTimeout(() => {
        category.style.display = "none";
      }, 500);
    }
  }
  public imagePath;
  imgURL: any;
  public message: string;
  msg;
  colr;
  constructor(private router: Router, private _fd: FetchDataService, private _auth: AuthService) { }

  ngOnInit(): void {
    // let category: any = document.getElementById("categoryRadio");
    // category.style.display = "none";

  }

  preview(event) {
    let files = event.target.files;
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    } else {
      const file = files[0];
      this.signupForm.patchValue({
        image: file
      });
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }

  isChecked(event) {
    this.checked = !this.checked;
  }
  allowNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  invalidphone: any;
  invalidMessage;
  invalidBoolean = false;
  register(data) {
    console.log('erespone', this.signupForm.value)
    const formData = new FormData();
    //  formData.append('password', this.signupForm.get('password').value);
    formData.append('name', this.signupForm.value.f_name+' '+this.signupForm.value.l_name);
    formData.append('email', this.signupForm.value.email);
    formData.append('mobile', this.signupForm.value.mobile);
    formData.append('company', this.signupForm.value.company_name);
    formData.append('designation', this.signupForm.value.designation);
    formData.append('city', this.signupForm.value.city);
    formData.append('gender', this.signupForm.value.gender);
    formData.append('is_founded', this.signupForm.value.is_founded);
    formData.append('tnc', this.signupForm.value.policy);
    // formData.append('is_checked', '1');

    //  formData.append('interests', this.signupForm.get('interests').value);
    // formData.append('token', '123');
    if (this.signupForm.valid) {
      this._auth.registerVEvent(formData).subscribe((res: any) => {
        if (res.code == 1) {
          this.successMsg = true;
          data.resetForm();
          // this.router.navigate(['/login']);
          // setTimeout(() => {
          //   this.router.navigate(['/login']);
          // }, 3000);
          setTimeout(() => {
            this.successMsg = false;

          }, 2000);
        }
        else {
          // alert(this.msg)
          this.invalidMessage = 'E-Mail address or Phone no. Already registered';
          this.failedMsg = true;
          this.msg = res.result;
          data.resetForm();
          
          setTimeout(() => {
            this.failedMsg = false;
          // this.msg = res.result;

          }, 2000);
        }
        // alert(this.msg)
        this.invalidMessage = '';
        this.invalidphone = 'E-Mail address or Phone no. Already registered';
        this.signupForm.reset();
      });
    } else {
      // alert("thisis msg")
      this.invalidBoolean = true;
      this.invalidphone = '';
      this.invalidMessage = 'All Fields are required *';
    }
  }
}
