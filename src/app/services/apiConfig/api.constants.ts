export const ApiConstants = {
    signup:'virtualapi/v1/auth/signup/event_id',
    login: 'virtualapi/v1/serdia/attendee/login',
    acmeLogin: 'virtualapi/v1/acma/attendees/login',
    // login: 'virtualapi/v1/auth/login/event_id/123/email/w@w.com/password/qwer/role_id/1',
    profile: 'virtualapi/v1/auth/profile/get',
    attendees: 'virtualapi/v1/get/attendees/event_id',
    addComment: 'virtualapi/v1/user/comment/add',
    chatAttendeesComments: 'virtualapi/v1/get/attendees',
    // one2oneCommentPost: 'virtualapi/v1/one2one/chat/request',
    one2oneCommentPost: 'virtualapi/v1/one2one/chat/post/event_id/139',
    One2OneCommentList: 'virtualapi/v1/one2one/chat/get',
    commentsList: 'virtualapi/v1/user/comment/grouplist',
    ratingMaster:'virtualapi/v1/rating/master/list',
    feedbackMaster:'virtualapi/v1/feedback/master/list',
    ratingPost: 'virtualapi/v1/rating/post',
    feebackPost:'virtualapi/v1/feedback/post',
    commentsLists: 'virtualapi/v1/user/comment/grouplist',
    askQuestion: 'virtualapi/v1/one2one/chat/post/event_id/139',
    helpdesk: 'virtualapi/v1/ask/question/post',
    askQuestionLive: 'virtualapi/v1/ask/live/question/post',
    getPolls: 'virtualapi/v1/get/poll/event_id/139/id',
    postPolls: 'virtualapi/v1/poll/answer/post',
   // getQuestionAnswser: 'virtualapi/v1/asked/question/answer/get/user_id',
    helpdeskAnswes: 'virtualapi/v1/asked/question/answer/get/user_id',
    getQuestionAnswser: 'virtualapi/v1/one2one/chat/get/receiver_id',
    // getliveAnswser: 'virtualapi/v1/get/live/questions/event_id/52/audi_id',
    wtsappAgendaFiles:'virtualapi/v1/get/document/event_id/139',
    quizlist: 'virtualapi/v1/get/quiz',
    submitQuiz: 'virtualapi/v1/quiz/answer/post',
    summaryQuiz: 'virtualapi/v1/get/quiz/summary',
    uploadPic: 'virtualapi/v1/upload/pic',
    logout: 'virtualapi/v1/auth/attendee/logout',
    groupChats: 'virtualapi/v1/get/mongo/groupchat/room',
    briefcaseList: 'virtualapi/v1/get/briefcase/list',
    postBriefcase: 'virtualapi/v1/addto/briefcase/post',
    exhibition:'virtualapi/v1/get/exhibitions/event_id/139',
    exhibitiontwo:'virtualapi/v1/get/exhibitions/event_id/139/id',
    standard : 'virtualapi/v1/get/exhibitions/event_id/139/category/standard',
    premium : 'virtualapi/v1/get/exhibitions/event_id/139/category/premium',
    stae_country: 'virtualapi/v1/get/exhibitions/event_id/139/category',

    // multiaudi:'virtualapi/v1/get/poll/event_id/52/audi_id/3'
    activeInactive :'virtualapi/v1/get/auditorium/event_id/139',
    postCardDrop: 'virtualapi/v1/card/drop/post',
    scheduleaCall:'virtualapi/v1/user/schedule/call',
    attendeSchedulecall:'virtualapi/v1/attendee/schedule/call',
    callListScheduled:'virtualapi/v1/user/schedule/call/list/user_id',
    callschdeuledAttendee:'virtualapi/v1/attendee/schedule/call/list/user_id',
    chatHistory:'virtualapi/v1/user/chat/history/event_id/139/user_id',
    brouchers:'virtualapi/v1/get/brochure/list/event_id/139/exhibition_id',
    getTimeSlots:'virtualapi/v1/get/brochure/list/event_id/139/exhibition_id'
}
