import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ApiConstants } from './apiConfig/api.constants';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.baseUrl;
  signup = ApiConstants.signup;
  login = ApiConstants.login;
  constructor(private http: HttpClient,private router:Router) { }
  httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
    //  'Content-Type':'application/json; charset=utf-8',
    //  'Host': '<calculated when request is sent>',
    //  'Content-Length':'<calculated when request is sent>',
     'Access-Control-Allow-Origin':'*'
    //  'Content-Type': 'multipart/form-data; boundary=<calculated when request is sent>',
     
    })
   };
   
  register(data: any){
    const token = 123;
    return this.http.post(`${this.baseUrl}/${this.signup}/${token}`, data);
  }
  registerVEvent(data:any){
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/user/register/event_id/180`, data);
  }
  // registerVEvent(data:any){
  //   return this.http.post(`${this.baseUrl}/virtualapi/v1/email/signup/event_id/139`, data);
  // }
  loginMethod(loginObj:any){
    return this.http.get(`${this.baseUrl}/${this.login}/event_id/${loginObj.event_id}/email/${loginObj.email}`);
  }

  loginExhibitors(user):Observable<any>{
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('email', user.email);
    params = params.set('password', user.password);

    return this.http.post(`${this.baseUrl}/virtualapi/v1/get/exhibitors/login/event_id/139`,params);
  }

  loginExhibit(users):Observable<any>{
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('email', users.email);
    params = params.set('password', users.password);

    return this.http.post(`${this.baseUrl}/virtualapi/v1/get/exhibitors/login/event_id/139`,params);
  }

  logout(user_id):Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.logout}/user_id/${user_id}`);
  }
  acmeLoggedinMethod(loginObj:any):Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.acmeLogin}/event_id/${loginObj.event_id}/email/${loginObj.email}/registration_number/${loginObj.registration_number}`);
  }
}
