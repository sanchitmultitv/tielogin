import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ExhibitionHallComponent } from './core-components/exhibition-hall/exhibition-hall.component';
import { RegistrationDeskComponent } from './core-components/registration-desk/registration-desk.component';
import { MeetingRoomComponent } from './core-components/meeting-room/meeting-room.component';
import { WelcomeLobbyComponent } from './core-components/welcome-lobby/welcome-lobby.component';
import {WebcamModule} from 'ngx-webcam';
import {SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { VisitorProfileComponent } from './core-components/visitor-profile/visitor-profile.component';
import { DirectloginComponent } from './core-components/directlogin/directlogin.component';

import { NgSelectModule } from '@ng-select/ng-select';
const data: SocketIoConfig ={ url : 'https://belive.multitvsolution.com:8030', options: {} };


@NgModule({
  declarations: [
    AppComponent,
    ExhibitionHallComponent,
    RegistrationDeskComponent,
    MeetingRoomComponent,
    WelcomeLobbyComponent,
    VisitorProfileComponent,
    DirectloginComponent
    
  ],
  imports: [
    NgSelectModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    WebcamModule,
    SocketIoModule.forRoot(data),
    ToastrModule.forRoot(),
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
